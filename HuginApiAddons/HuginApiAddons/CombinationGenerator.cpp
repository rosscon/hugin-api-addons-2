#include "StdAfx.h"
#include "HuginApiAddons.h"
#include <list>
#include <iostream>
#include <iterator>

namespace ApiAddons {

	list<list<int>> CombinationGenerator::GenerateCombinations(list<int> sizes)
	{
		list<list<int>> combinations;

		int totalValues = sizes.front();
		list<int> colValues;

		for(int i = 0; i < sizes.size(); i++)
		{
			colValues.push_back(0);
		}

		bool done = false;
		bool nextInc = false;

		while(!done)
		{
			colValues.back()++;

			if(colValues.back() == sizes.back())
			{
				colValues.back() = 0;
				nextInc = true;
			}
			else
			{
				nextInc = false;
			}

			for(int i = colValues.size() - 2; (nextInc && i > -1); i--)
			{
				list<int>::iterator itc = colValues.begin();
				std::advance(itc, i);

				list<int>::iterator its = sizes.begin();
				std::advance(its, i);

				int s = *its;

				int colValue = *itc;
				colValue++;

				if(colValue == s)
				{
					colValue = 0;
					*itc = 0;
				}
				else
				{
					*itc = colValue;
					nextInc = false;
				}
			}

			done = true;

			for(int i = 0; i < colValues.size(); i++)
			{
				list<int>::iterator itc = colValues.begin();
				std::advance(itc, i);

				if(*itc > 0) done = false;
			}

			list<int> copyList = colValues;

			//std::copy(colValues.begin(), colValues.end(), copyList);

			combinations.push_back(copyList);
		}

		return combinations;
	}
}
