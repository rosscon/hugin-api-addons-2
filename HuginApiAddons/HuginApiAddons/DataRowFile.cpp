#include "StdAfx.h"
#include "DataRowFile.h"


DataRowFile::DataRowFile(void)
{
}


//////////////////////////////////////////////////////////////////////////
//Converts an input CSV file into data rows
//////////////////////////////////////////////////////////////////////////
list<DataRow> DataRowFile::FileToRows(string inputFile, string stateCol, string obsCol, string actionCol)
{
	csvCalculations csvCalc;
	list<string> sRows = csvCalc.FileToRows(inputFile);

	string headders = sRows.front();
	sRows.pop_front();

	//Determine col header indexes
	int stateIndex = 0;
	int obsIndex = 0;
	int actionindex = 0;

	int i = 0;
	while (headders.find(",", 0) != string::npos)
	{
		size_t  pos = headders.find(",", 0);
		string temp = headders.substr(0, pos);
		headders.erase(0, pos + 1);
		if(temp == stateCol)
		{
			stateIndex = i;
		}
		else if(temp == obsCol)
		{
			obsIndex = i;
		}else if(temp == actionCol)
		{
			actionindex = i;
		}

		i++;
	}

	list<DataRow> rows;

	for(list<string>::iterator it = sRows.begin(); it != sRows.end(); it++)
	{
		string sRow = *it;
		string original = sRow;
		DataRow dRow;
		dRow.originalRow = original;

		i = 0;
		while (sRow.find(",", 0) != string::npos)
		{
			size_t  pos = sRow.find(",", 0);
			string temp = sRow.substr(0, pos);
			sRow.erase(0, pos + 1);
			if(i == stateIndex)
			{
				dRow.state = temp;
			}
			else if(i == obsIndex)
			{
				dRow.observation = temp;
			}else if(i == actionindex)
			{
				dRow.action = temp;
			}
			i++;
		}
		dRow.isLandmark = false;
		rows.push_back(dRow);
	}

	return rows;
}

//////////////////////////////////////////////////////////////////////////
//Saves rows to an output csv file
//////////////////////////////////////////////////////////////////////////
void DataRowFile::RowsToFile(string outputFile, list<DataRow> rows, string stateCol, string obsCol, string actionCol)
{
	ofstream outFile;
	outFile.open(outputFile.c_str());
	outFile << stateCol << "," << obsCol << "," << actionCol << "," << "Landmark,HistoryLength,History" << endl;

	for(list<DataRow>::iterator it = rows.begin(); it != rows.end(); it++)
	{
		DataRow row = *it;		

		//Print history to next column, generate using a reverse iterator
		stringstream ss;
		list<DataRow>::iterator ith = it;
		for(int i = 0; i < row.historyLength; i++)
		{
			ith--;
		}

		for(list<DataRow>::iterator itht = ith; itht!=it; itht++)
		{
			DataRow tempRow = *itht;

			if(itht != ith)
			{
				ss << tempRow.observation << "-" << tempRow.action;
			}
			else
			{
				ss << tempRow.action;
			}

			ss << "-";
		}

		if(row.historyLength > 0)
		{
			ss << row.observation;
		}

		outFile << row.state << "," << row.observation << "," << row.action << "," << row.isLandmark << "," << row.historyLength << "," << ss.str() << endl;
	}

	outFile.close();
}

void DataRowFile::OriginalRowsToFile(string outputFile, list<DataRow> rows, string headers)
{
	ofstream outFile;
	outFile.open(outputFile.c_str());
	outFile << headers << ",HistoryLength,History," << endl;

	for(list<DataRow>::iterator it = rows.begin(); it != rows.end(); it++)
	{
		DataRow row = *it;		

		//Print history to next column, generate using a reverse iterator
		stringstream ss;
		list<DataRow>::iterator ith = it;
		for(int i = 0; i < row.historyLength; i++)
		{
			ith--;
		}

		for(list<DataRow>::iterator itht = ith; itht!=it; itht++)
		{
			DataRow tempRow = *itht;

			if(itht != ith)
			{
				ss << tempRow.observation << "-" << tempRow.action;
			}
			else
			{
				ss << tempRow.action;
			}

			ss << "-";
		}

		if(row.historyLength > 0)
		{
			ss << row.observation;
		}

		outFile << row.originalRow << row.historyLength << "," << ss.str() << "," << endl;
	}

	outFile.close();
}
