#pragma once
#include <list>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "csvCalculations.h"

using namespace std;

struct DataRow
{
	string state;
	string observation;
	string action;
	bool isLandmark;
	int historyLength;
	string originalRow;
	int time;
};

class DataRowFile
{
public:
	__declspec(dllexport) DataRowFile(void);
	__declspec(dllexport) list<DataRow> DataRowFile::FileToRows(string inputFile, string stateCol, string obsCol, string actionCol);
	__declspec(dllexport) void DataRowFile::RowsToFile(string outputFile, list<DataRow> rows, string stateCol, string obsCol, string actionCol);
	__declspec(dllexport) void DataRowFile::OriginalRowsToFile(string outputFile, list<DataRow> rows, string headers);
};

