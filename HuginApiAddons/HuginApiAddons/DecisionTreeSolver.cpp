#include "StdAfx.h"
#include "HuginApiAddons.h"

namespace ApiAddons {

	//simply opend the domain from file and passes it on
	double* DecisionTreeSolver::Solve(string netFileName)
	{
		DefaultParseListener pl;
		Domain domain(netFileName, &pl);
		string logFileName = netFileName + ".log";
		FILE * logFile = fopen(logFileName.c_str(), "w");

		if (logFile == NULL)
		{
			cerr << "Could not open \"" << logFileName << "\"\n";
			exit(EXIT_FAILURE);
		}

		domain.setLogFile(logFile);
		domain.triangulate(H_TM_FILL_IN_WEIGHT);
		domain.compile();
		domain.setLogFile(NULL);
		fclose(logFile);

		return Solve(&domain);
	}

	double* DecisionTreeSolver::Solve(Domain * domain)
	{
		DomainPrinter dp;
		dp.PrintDomain(domain);

		DomainToDecisionTreeConverter converter;
		vector<DtNode*> tree = converter.Convert(domain);

		return 0;
	}
}
