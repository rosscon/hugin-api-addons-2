# include "Stdafx.h"
# include "HuginApiAddons.h"
# include <iostream>
# include <cmath>

namespace ApiAddons
{
	void DomainPrinter::PrintDomain(Domain * domain)
	{
		NodeList nodes = domain->getNodes();

		for (NodeList::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
		{
			Node *node = *it;

			Category category = node->getCategory();
			char type = (category == H_CATEGORY_CHANCE ? 'C' : category == H_CATEGORY_DECISION ? 'D' : 'U');

			cout << "\n[" << type << "] " << node->getLabel() << " (" << node->getName() << ")\n";

			if (category == H_CATEGORY_UTILITY)
			{
				UtilityNode *uNode = dynamic_cast<UtilityNode*> (node);
				cout << "  - Expected utility: " << uNode->getExpectedUtility() << endl;
			}
			else if (node->getKind() == H_KIND_DISCRETE)
			{
				DiscreteNode *dNode = dynamic_cast<DiscreteNode*> (node);

				for (size_t i = 0, n = dNode->getNumberOfStates(); i < n; i++)
				{
					cout << "  - " << dNode->getStateLabel(i) << " " << dNode->getBelief(i);
					cout << " (" << dNode->getExpectedUtility(i) << ")";
					cout << endl;
				}
			}
			else
			{
				ContinuousChanceNode *ccNode = dynamic_cast<ContinuousChanceNode*> (node);
				cout << "  - Mean : " << ccNode->getMean() << endl;
				cout << "  - SD   : " << sqrt(ccNode->getVariance()) << endl;
			}
		}
	}
}
