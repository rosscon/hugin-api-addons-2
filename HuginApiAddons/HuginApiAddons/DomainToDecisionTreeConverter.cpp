#include "StdAfx.h"
#include "HuginApiAddons.h"

//////////////////////////////////////////////////////////////////////////
//Author	:	Ross Conroy	ross.conroy@tees.ac.uk
//Date		:	06/05/2014
//
//This class converts an influence diagram into a decision tree structure
//Algorithm
//	1.Loop for all nodes
//		1.1.Find root node of ID (a node with no parent nodes), if more
//		than one and a decision node is available then choose the decision
//		node.
//		1.2.Convert node to a decision tree node
//			1.2.1.If Decision Node
//				1.2.1.1. TODO convert instructions
//			1.2.2.If Chance Node
//				1.2.2.1. TODO convert instructions
//			1.2.3.If Utility/Value Node
//				1.2.3.1. TODO convert instructions
//		1.3.Add Converted node to tree by adding to all current leaf nodes
//		1.4.Remove node from ID.
//////////////////////////////////////////////////////////////////////////

using namespace ApiAddons;

vector<DtNode*> DomainToDecisionTreeConverter::Convert(Domain * domain)
{
	vector<DtNode*> tree;
	return tree;
}

/*Node DomainToDecisionTreeConverter::GetRootNode(Domain * domain)
{
	return NULL;
}

DtNode DomainToDecisionTreeConverter::ConvertToDecisionTreeNode(Node * node)
{
	return NULL;
}

long* DomainToDecisionTreeConverter::GetCombinationFromParents(DtNode dtNode, Node node, vector<Node*> parents)
{
	return NULL;
}

int DomainToDecisionTreeConverter::GetArrayStartPosition(long* combination, vector<Node*> parents, int width)
{
	return NULL;
}

void DomainToDecisionTreeConverter::AddDataTables(list<DtNode*> tree, Domain * domain)
{

}*/