#pragma once

#include "hugin"
#include <iostream>

using namespace HAPI;
using namespace std;

class ExpandTimeSteps
{
public:
	__declspec(dllexport) enum IdType{LIMID, DID, IDID, NOFORGET};
	__declspec(dllexport) ExpandTimeSteps(void);
	__declspec(dllexport) Domain * Expand(Domain * inputID, int noTimeSteps, IdType idTpye);	

	__declspec(dllexport) int FindLastTimeStep (Domain * inputID);
	__declspec(dllexport) int FindFirstTimeStep (Domain * inputID);
	__declspec(dllexport) NodeList GetNodesForTimeStep(Domain * inputID, int timeStep);

private:
	
	NodeList GetNodesForNextTimeStep(NodeList previousNodes, Domain * inputID);
	string GetNameForNextTimeStep(string previousName);
	void SetParentsForNextStep(NodeList previousNodes, IdType idTpye);
	void SetCPTsForNextStep(NodeList previousNodes, IdType idTpye);
	int GetTimeStepFromNodeName(string inNodeName);
	int GapBetweenSteps;
	bool isAgentJActionNodeName(string nodeName);
};

