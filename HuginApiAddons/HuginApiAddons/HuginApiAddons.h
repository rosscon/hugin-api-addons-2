// ApiAddons.h

#pragma once

#include "hugin"
#include <iostream>
#include <string>
#include <list>
#include <cmath>
#include <vector>

using namespace HAPI;
using namespace std;

namespace ApiAddons {

	class DeltaAj
	{
	public:
		__declspec(dllexport) double* CalculateDeltaEv(double deltaAj, Domain *domain, string agentJ);
		__declspec(dllexport) double* CalculateDeltaEv(double deltaAj, string netFileName, string agentJ);
	private:
		double* CalculateSingleNode(double deltaAj, Domain * domain, Node *agentNode);
		double* CalculateMultipleNode(double deltaAj, Domain * domain, list<Node*> agentNodes);
		list<NumberList> GetStartingTables(list<Node*> agentNodes);
		void ResetStartingTables(list<NumberList>, list<Node*> agentNodes);
	};


	class DomainPrinter
	{
	public:
		__declspec(dllexport) void PrintDomain(Domain *domain);
	};


	class CombinationGenerator
	{
	public:
		__declspec(dllexport) list<list<int>> GenerateCombinations(list<int> sizes);
	};


	class DecisionTreeSolver
	{
	public:
		__declspec(dllexport) double* Solve(Domain * domain);
		__declspec(dllexport) double* Solve(string netFileName);
	};	
}

namespace ApiAddons
{
	class DtNode
	{
	public:
		string GetName();
	};

	class DomainToDecisionTreeConverter
	{
	public:
		vector<DtNode*> Convert(Domain * domain);
		//private:
		//	Node GetRootNode(Domain * domain);
		//	DtNode ConvertToDecisionTreeNode(Node * node);
		//	double* GetCombinationFromParents(DtNode dtNode, Node node, vector<Node*> parents);
		//	int GetArrayStartPosition(long* combination, vector<Node*> parents, int width);
		//	void AddDataTables(list<DtNode*> tree, Domain * domain);
	};
}




