#pragma once
#include <list>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "DataRowFile.h"

using namespace std;

class IdentifyHistories
{
public:
	__declspec(dllexport) IdentifyHistories(void);

	__declspec(dllexport) void IdentifyHistories::Identify(string inputFile, string outputFile, string stateCol, string obsCol, string actionCol, int maxHistory);
	__declspec(dllexport) list<DataRow> IdentifyHistories::Identify(list<DataRow> dataRows, int maxHistory);
};
