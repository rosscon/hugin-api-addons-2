#pragma once
#include <list>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "DataRowFile.h"

using namespace std;

class IdentifyLandmarks
{
public:
	__declspec(dllexport) IdentifyLandmarks(void);	

	__declspec(dllexport) void IdentifyLandmarks::ResetActions(string inputFile, string outputFile, list<string> resetActions, string stateCol, string obsCol, string actionCol);
	__declspec(dllexport) list<DataRow> IdentifyLandmarks::ResetActions(list<DataRow> dataRows, list<string> resetActions);

	__declspec(dllexport) void IdentifyLandmarks::UniqueState(string inputFile, string outputFile, string stateCol, string obsCol, string actionCol);
	__declspec(dllexport) list<DataRow> IdentifyLandmarks::UniqueState(list<DataRow> dataRows);

	__declspec(dllexport) void IdentifyLandmarks::ZeroHistoryAction(string inputFile, string outputFile, string stateCol, string obsCol, string actionCol, int maxHistory);
	__declspec(dllexport) list<DataRow> IdentifyLandmarks::ZeroHistoryAction(list<DataRow> dataRows, int maxHistory);

};

