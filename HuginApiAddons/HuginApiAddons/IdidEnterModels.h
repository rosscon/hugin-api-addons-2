#pragma once

#include "hugin"
#include <list>
#include <cstdlib>
#include <iostream>
#include "ExpandTimeSteps.h"
#include <hash_map>
#include <fstream>
#include <sstream>
#include "HuginApiAddons.h"
#include <vector>
#include "PolicyNode.h"
#include "PolicyTreeBuilder.h"

using namespace ApiAddons;

class IdidEnterModels
{
private:
	//Domain * IdidEnterModels::ExpandModelNodesSteps(Domain * inputIDID, string ModelPrefix, int numModels);
	Domain * IdidEnterModels::UpdateObservationNodesTables(Domain * inputIDID, list<Domain *> models, string jObservationPrefix, int numModels);
	Domain * IdidEnterModels::UpdateActionNodesTables(Domain * inputIDID, vector<PolicyNode*> policyRoots, string jActionPrefix);	

	list<string> IdidEnterModels::CsvListToListOfStrings(string input);

	//OcurancesOfAction IdidEnterModels::OcurancesOfActionFromPpr(PPR ppr);
public:
	__declspec(dllexport) IdidEnterModels(void);

	__declspec(dllexport) Domain * IdidEnterModels::ExpandModelNodesSteps(Domain * inputIDID, string ModelPrefix, int numModels, vector<string> modelNames);

	__declspec(dllexport) Domain * IdidEnterModels::EnterDidModelsIntoIdid(Domain * inputIDID, list<Domain *> models, string statePrefix, 
																string utilityPrefix, string iObservationPrefix, string jObservationPrefix, 
																string iActionPrefix, string jActionPrefix, string modelPrefix);

	/*__declspec(dllexport) Domain * IdidEnterModels::EnterPprModelsIntoIdid(Domain * inputIDID, hash_map<string, PPR> pprs, string statePrefix, 
																string utilityPrefix, string iObservationPrefix, string jObservationPrefix, 
																string iActionPrefix, string jActionPrefix, string modelPrefix, int NumModels);*/
};

