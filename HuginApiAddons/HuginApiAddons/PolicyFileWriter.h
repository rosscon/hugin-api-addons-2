#pragma once

#include "hugin"
#include <iostream>
#include <sstream>
#include <hash_map>

#include <list>
#include "HuginApiAddons.h"
#include <fstream>
#include "ExpandTimeSteps.h"

using namespace HAPI;
using namespace std;
using namespace stdext;
using namespace ApiAddons;

class PolicyFileWriter
{
private:
	list<string> PolicyFileWriter::GetBestDecision(DiscreteDecisionNode* node);
	void PolicyFileWriter::WriteToFile(string folder, string nodeName, string extension, hash_map<string, list<string>> policy);
public:
	//__declspec(dllexport) enum OUTPUT_FILE_TYPE{SINGLE_FILE, MULTIPLE_FILES};
	__declspec(dllexport) PolicyFileWriter(void);
	__declspec(dllexport) bool PolicyFileWriter::WritePolicyToFile(Domain * inputDID, string actionPrefix, string observationPrefix, string outputFolder, string fileExt);
};

