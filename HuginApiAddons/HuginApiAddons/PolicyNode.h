#pragma once

#include <list>
#include <map>
#include <hash_map>
#include <iostream>
#include <string>
#include <sstream>

using namespace std;
using namespace stdext;

class PolicyNode
{
private:
	string _name;
	list<string> _observations;
	hash_map<string, PolicyNode*> _children;
	PolicyNode * _parent;

public:
	__declspec(dllexport) PolicyNode(void);

	__declspec(dllexport) PolicyNode(string name, list<string> observations, PolicyNode * parent);

	__declspec(dllexport) bool PolicyNode::IsLeafNode();
	__declspec(dllexport) bool PolicyNode::IsFinalNode();
	__declspec(dllexport) bool PolicyNode::IsRootNode();
	__declspec(dllexport) int PolicyNode::GetTimeStep();
	__declspec(dllexport) list<string> PolicyNode::GetParentsCombinations(PolicyNode* self);
	__declspec(dllexport) void PolicyNode::SetNodeForObservation(string observation, PolicyNode * node);

	__declspec(dllexport) string PolicyNode::GetActionForHistory(list<string> input);
};

