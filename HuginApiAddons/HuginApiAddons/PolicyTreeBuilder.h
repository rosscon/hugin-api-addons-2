#pragma once
#include "hugin"
#include "PolicyNode.h"
#include <iostream>
#include <sstream>
#include "ExpandTimeSteps.h"

using namespace HAPI;
using namespace std;
using namespace stdext;

class PolicyTreeBuilder
{
private:
	string PolicyTreeBuilder::GetBestDecision(DiscreteDecisionNode* node);
	Domain * PolicyTreeBuilder::SetDomainHistory(Domain * inputDID, list<string> history, string observationPrefix, string actionPrefix);
public:
	__declspec(dllexport) PolicyTreeBuilder(void);
	__declspec(dllexport) PolicyNode * PolicyTreeBuilder::ConvertToPolicyTree(Domain * inputDID, string actionPrefix, string observationPrefix);
};

