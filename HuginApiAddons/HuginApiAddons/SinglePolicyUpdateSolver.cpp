#include "StdAfx.h"
#include "SinglePolicyUpdateSolver.h"
#include <iostream>
#include <sstream>

//////////////////////////////////////////////////////////////////////////
//Author	:	Ross Conroy ross.conroy@tees.ac.uk
//Date		:	06/05/2014
//
//Uses the single policy update algorithm to generate a policy for the
//ID based on inserted evidence
//////////////////////////////////////////////////////////////////////////

//Domain constructor
SinglePolicyUpdateSolver::SinglePolicyUpdateSolver(Domain * inputID)
{
	_domain = inputID;
	_domain->compile();
	_domain->updatePolicies();
}

//net file location constructor
SinglePolicyUpdateSolver::SinglePolicyUpdateSolver(string inputIDLocation)
{
	DefaultParseListener pl;
	_domain = new Domain(inputIDLocation, &pl);
	_domain->compile();
	_domain->updatePolicies();
}

//solve ID - Cheating by using update policies HAPI call
//TODO inplement SPU
Domain * SinglePolicyUpdateSolver::solve()
{
	//_domain->updatePolicies();
	return _domain;
}

//Insert evidence into ID based on nodemane, timestep and evidence state name
//Finds the node and state pointer to insert evidence to
bool SinglePolicyUpdateSolver::InsertEvidence(string nodeName, int timestep, string evidence)
{
	try
	{
		DiscreteNode * node = (DiscreteNode*)_domain->getNodeByName(nodeName);
		int evidenctPtr = node->getStateIndex(evidence);
		return InsertEvidence(node, evidenctPtr);
	}
	catch (ExceptionHugin e)
	{
		cout << e.what() << endl;
		return false;
	}
}

bool SinglePolicyUpdateSolver::InsertEvidence(DiscreteNode * node, int evidenceState)
{
	try
	{
		node->selectState(evidenceState);
		return true;
	}
	catch (ExceptionHugin e)
	{
		cout << e.what() << endl;
		return false;
	}
}

Domain * SinglePolicyUpdateSolver::GetDomain()
{
	return _domain;
}
