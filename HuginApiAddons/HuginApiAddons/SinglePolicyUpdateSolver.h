#pragma once

#include "hugin"

using namespace HAPI;
using namespace std;

class SinglePolicyUpdateSolver
{
public:
	__declspec(dllexport) SinglePolicyUpdateSolver(Domain * inputID);
	__declspec(dllexport) SinglePolicyUpdateSolver(string inputIDLocation);
	__declspec(dllexport) Domain * solve();
	__declspec(dllexport) bool InsertEvidence(string nodeName, int timestep, string evidence);
	__declspec(dllexport) bool InsertEvidence(DiscreteNode * node, int evidenceState);
	__declspec(dllexport) Domain * GetDomain();
private:
	Domain * _domain;
};

