#pragma once
#include <list>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "DataRowFile.h"
#include "IdentifyHistories.h"

using namespace std;

class StarCraftIdentifyHistories
{
private:
	hash_map<int, list<DataRow>> StarCraftIdentifyHistories::SeparateRowsByUnitID(string inputFile, string stateCol, string obsCol, string actionCol, string unitIDCol, string timeCol);
	list<DataRow> StarCraftIdentifyHistories::CompressTimeSteps(list<DataRow> inputRows, int timeStepDuration);
	int StarCraftIdentifyHistories::GetColIndex(string header, string unitIDCol);
	int StarCraftIdentifyHistories::GetColIntData(string original, int colIndex);
public:
	__declspec(dllexport) StarCraftIdentifyHistories(void);
	__declspec(dllexport) void StarCraftIdentifyHistories::Identify(string inputFile, string outputFile, string stateCol, string obsCol, string actionCol, string unitIDCol, string timeCol, int maxHistory);
};

