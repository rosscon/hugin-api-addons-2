#pragma once
#include <list>
#include <map>
#include <hash_map>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <boost/filesystem.hpp>

using namespace std;
using namespace stdext;
namespace fs = ::boost::filesystem;

class csvCalculations
{
private:
	list<string> csvCalculations::GetFiles(string folder);	
	list<double> csvCalculations::GetData(list<string> rows, string colHeader);
public:
	__declspec(dllexport) csvCalculations(void);

	__declspec(dllexport) hash_map<string, double> csvCalculations::CalculateAveragesForFolder(string folder, string column);
	__declspec(dllexport) hash_map<string, double> csvCalculations::CalculateAveragesForFolder(string folder, string column, double min, double max);
	__declspec(dllexport) double csvCalculations::AverageForFile(string file, string colHeader);
	__declspec(dllexport) double csvCalculations::AverageForFile(string file, string colHeader, double minVal, double maxVal);

	__declspec(dllexport) hash_map<string, hash_map<double, int>> csvCalculations::CalculateFrequenciesForFolder(string folder, string column);
	__declspec(dllexport) hash_map<double, int> csvCalculations::CalculateFrequenciesForFile(string folder, string column);

	__declspec(dllexport) void csvCalculations::MergeFilesInFolder(string inputFolder, int chars, string outputFolder);

	__declspec(dllexport) list<string> csvCalculations::FileToRows(string file);
};

