#pragma once

#include "hugin"
#include <List>
#include <iostream>
#include <iosfwd>
#include <sstream>

using namespace HAPI;
using namespace std;

class iDIDSolver
{
public:
	__declspec(dllexport) iDIDSolver(void);
	__declspec(dllexport) Domain * iDIDSolver::Solve(Domain * inputIDID, list<Domain *> models, int numExpansions);
private:
	void iDIDSolver::SetModelNodesNumbers(NodeList modelNodes, int numModels);
	NodeList iDIDSolver::GetModelNodes(Domain * inputDID);
	NodeList iDIDSolver::GetModelActionNodes(Domain * inputDID);
	NodeList iDIDSolver::GetModelObservationNodes(Domain * inputDID);
	list<Domain *> iDIDSolver::ExpandModels(list<Domain *> models, int expandSteps);
	Domain * iDIDSolver::AddModelPoliciesToIDID(Domain * inputIDID, list<Domain *> models);
	string iDIDSolver::GetModelEquivilentDecisionNodeName(string modelNodeName);
};

