﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using HAPI;
using HuginApiAddonsCS.Policy;
using HuginApiAddonsCS.Policy.PolicyGenerator;
using HuginApiAddonsCS.Simulator;

namespace HuginApiAddonsCS.Extensions
{
    public static class DomainExtensions
    {
        private static float _weighting = 1.0f;

        public static void SetWeighting(this Domain domain, float weighting)
        {
            if (weighting > 0)
            {
                _weighting = weighting;
            }
        }

        public static float GetWeighting(this Domain domain)
        {
            return _weighting;
        }

        /// <summary>
        /// Gets the first time step value from the domain
        /// </summary>
        /// <param name="domain"></param>
        /// <returns>value of first time step</returns>
        public static int GetFirstTimeStep(this Domain domain)
        {
            int min = -1;

            foreach (Node node in domain.GetNodes())
            {
                int t = node.GetNodeTimeStep();
                if (t < min || min == -1)
                {
                    min = t;
                }
            }

            return min;
        }

        /// <summary>
        /// Gets the value of the final time step
        /// </summary>
        /// <param name="domain"></param>
        /// <returns>the value of the final time step</returns>
        public static int GetLastTimeStep(this Domain domain)
        {
            int max = -1;

            foreach (Node node in domain.GetNodes())
            {
                int t = node.GetNodeTimeStep();
                if (t > max)
                {
                    max = t;
                }
            }

            return max;
        }

        /// <summary>
        /// Gets a list of all the prefixes in the domain
        /// </summary>
        /// <param name="domain"></param>
        /// <returns>list of prefixes</returns>
        public static List<string> GetPrefixes(this Domain domain)
        {
            Dictionary<string, string> domainPrefixes = new Dictionary<string, string>();

            foreach (Node node in domain.GetNodes())
            {
                string nodeName = node.GetNodePrefix();
                if (!domainPrefixes.ContainsKey(nodeName))
                {
                    domainPrefixes.Add(nodeName, nodeName);
                }
            }

            return new List<string>(domainPrefixes.Keys);
        }

        /// <summary>
        /// Gets the next node in the domain based on the current node
        /// </summary>
        /// <param name="domain">Domain extended</param>
        /// <param name="node">Node</param>
        /// <returns>The next node</returns>
        public static Node GetNextStepNode(this Domain domain, Node node)
        {
            return domain.GetNodeByName(node.GetNodePrefix() + (node.GetNodeTimeStep() + 1));
        }

        /// <summary>
        /// Gets the policy tree associated with theis domain
        /// </summary>
        /// <param name="domain">domain</param>
        /// <param name="ajPrefix">action prefix</param>
        /// <param name="ojPrefix">observation prefix</param>
        /// <returns>generated policy tree</returns>
        public static PolicyNode GetPolicyTree(this Domain domain, string ajPrefix, string ojPrefix)
        {
            PolicyGenerator generator = new PolicyGenerator();
            return generator.GeneratePolicyTree(domain, ajPrefix, ojPrefix);
        }

        /// <summary>
        /// Executes a single simulation on the domain
        /// </summary>
        /// <param name="domain">domain</param>
        /// <param name="ajPrefix">action prefix</param>
        /// <param name="ojPrefix">observation prefix</param>
        /// <param name="sPrefix">state prefix</param>
        /// <param name="uPrefix">utility prefix</param>
        /// <param name="model">model name</param>
        /// <returns>list of results</returns>
        public static List<SimulationResult> GetSimulationResultDid(this Domain domain, string ajPrefix, string ojPrefix,
            string sPrefix, string uPrefix, string model, Random rand)
        {
            List<SimulationResult> results = new List<SimulationResult>();

            List<uint> stateHistory = new List<uint>();

            for (int i = GetFirstTimeStep(domain); i <= GetLastTimeStep(domain); i++)
            {
                SimulationResult result = new SimulationResult();
                result.model_j = model;
                result.timeStep = i;

                DiscreteChanceNode stateNode = (DiscreteChanceNode) domain.GetNodeByName(sPrefix + i);
                uint stateIndex = stateNode.SetRandomState(rand);
                stateHistory.Add(stateIndex);
                result.state = stateNode.GetStateLabel(stateIndex);

                domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                if (i != GetFirstTimeStep(domain))
                {
                    DiscreteChanceNode observationNode = (DiscreteChanceNode)domain.GetNodeByName(ojPrefix + i);
                    uint obsIndex = observationNode.SetRandomState(rand);
                    result.observation_j = observationNode.GetStateLabel(obsIndex);
                }
                domain.RetractStateHistories(sPrefix);
                domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                DiscreteDecisionNode decisionNode = (DiscreteDecisionNode) domain.GetNodeByName(ajPrefix + i);
                uint decIndex = decisionNode.SetStateWithHighestReward();
                result.action_j = decisionNode.GetStateLabel(decIndex);

                domain.ResetStateHistories(stateHistory, sPrefix);
                domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                UtilityNode utilityNode = (UtilityNode)domain.GetNodeByName(uPrefix + i);
                result.util_j = utilityNode.GetExpectedUtility();
                
                results.Add(result);
            }

            domain.RetractFindings();
            return results;
        }

        /// <summary>
        /// Simulates an I-DID domain by including simulating a DID domain
        /// Simultaneously
        /// </summary>
        /// <param name="domain">domain</param>
        /// <param name="jDomain">agent j domain</param>
        /// <param name="ajPrefix">agent j action prefix</param>
        /// <param name="ojPrefix">agent j observation prefix</param>
        /// <param name="aiPrefix">agent i action prefix</param>
        /// <param name="oiPrefix">agent i observation prefix</param>
        /// <param name="sPrefix">state prefix</param>
        /// <param name="uPrefix">utility</param>
        /// <param name="model">model</param>
        /// <returns>results</returns>
        public static List<SimulationResult> GetSimulationResultIdid(this Domain domain, Domain jDomain, string ajPrefix, string ojPrefix,
            string aiPrefix, string oiPrefix, string sPrefix, string uPrefix, string model, Random rand, float errorValue, bool haltError)
        {
            List<SimulationResult> results = new List<SimulationResult>();

            List<uint> stateHistory = new List<uint>();
            List<uint> ajHistory = new List<uint>();

            bool error = false;

            for (int i = GetFirstTimeStep(domain); i <= GetLastTimeStep(domain) && !error; i++)
            {
                SimulationResult result = new SimulationResult();
                result.model_j = model;
                result.timeStep = i;

                try
                {
                    DiscreteChanceNode stateNodeI = (DiscreteChanceNode)domain.GetNodeByName(sPrefix + i);
                    DiscreteChanceNode stateNodeJ = (DiscreteChanceNode)jDomain.GetNodeByName(sPrefix + i);
                    uint stateIndex = stateNodeI.SetRandomState(rand);
                    stateNodeJ.SelectState(stateIndex);
                    stateHistory.Add(stateIndex);
                    result.state = stateNodeI.GetStateLabel(stateIndex);

                    domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);
                    jDomain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                    if (i != GetFirstTimeStep(domain))
                    {
                        DiscreteChanceNode observationNodeI = (DiscreteChanceNode)domain.GetNodeByName(oiPrefix + i);
                        DiscreteChanceNode observationNodeJ = (DiscreteChanceNode)jDomain.GetNodeByName(ojPrefix + i);
                        uint obsIndexJ = observationNodeJ.SetRandomState(rand);
                        uint obsIndexI = observationNodeI.SetRandomState(rand);
                        result.observation_i = observationNodeI.GetStateLabel(obsIndexI);
                        result.observation_j = observationNodeJ.GetStateLabel(obsIndexJ);
                    }
                    domain.RetractStateHistories(sPrefix);
                    domain.RetractStateHistories(ajPrefix);
                    jDomain.RetractStateHistories(sPrefix);

                    domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);
                    jDomain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                    DiscreteDecisionNode decisionNodeI = (DiscreteDecisionNode)domain.GetNodeByName(aiPrefix + i);
                    DiscreteDecisionNode decisionNodeJ = (DiscreteDecisionNode)jDomain.GetNodeByName(ajPrefix + i);

                    uint decIndexI = decisionNodeI.SetStateWithHighestReward();
                    uint decindexJ = decisionNodeJ.SetStateWithHighestReward();

                    ajHistory.Add(decindexJ);

                    result.action_i = decisionNodeI.GetStateLabel(decIndexI);
                    result.action_j = decisionNodeJ.GetStateLabel(decindexJ);

                    jDomain.ResetStateHistories(stateHistory, sPrefix);
                    domain.ResetStateHistories(stateHistory, sPrefix);
                    domain.ResetStateHistories(ajHistory, ajPrefix);

                    domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                    UtilityNode utilityNode_i = (UtilityNode)domain.GetNodeByName(uPrefix + i);
                    result.util_i = utilityNode_i.GetExpectedUtility();

                    jDomain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);

                    UtilityNode utilityNode_j = (UtilityNode)jDomain.GetNodeByName(uPrefix + i);
                    result.util_j = utilityNode_j.GetExpectedUtility();
                }
                catch (ExceptionHugin eh)
                {
                    result.util_i = errorValue;
                    result.util_j = errorValue;
                    //result.util_i = errorValue * (i - GetLastTimeStep(domain));
                    //result.util_j = errorValue * (i - GetLastTimeStep(domain));
                    //return new List<SimulationResult>();
                    //Todo make final step on error
                    error = true;
                    if (!haltError)
                    {
                        error = false;
                    }
                }

                results.Add(result);
            }

            //domain.ResetInferenceEngine();
            //jDomain.ResetInferenceEngine();
            domain.RetractFindings();
            jDomain.RetractFindings();
            domain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);
            jDomain.Propagate(Domain.Equilibrium.H_EQUILIBRIUM_SUM, Domain.EvidenceMode.H_EVIDENCE_MODE_NORMAL);
            return results;
        }

        //public static List<SimulationResult> GetSimulationResultIdid(this Domain domain, Domain jDomain, string ajPrefix,
        //    string ojPrefix, string aiPrefix, string oiPrefix, string sPrefix, string uPrefix, string model, Random rand)
        //{
        //    return domain.GetSimulationResultIdid(jDomain, ajPrefix, ojPrefix, aiPrefix, oiPrefix, sPrefix, uPrefix,
        //        model, rand, -200.0f);
        //}

        /// <summary>
        /// retracts all state evidence
        /// </summary>
        /// <param name="domain">domain</param>
        /// <param name="sPrefix">state prefix</param>
        public static void RetractStateHistories(this Domain domain, string sPrefix)
        {
            for (int i = GetFirstTimeStep(domain); i <= GetLastTimeStep(domain); i++)
            {
                DiscreteChanceNode stateNode = (DiscreteChanceNode)domain.GetNodeByName(sPrefix + i);
                stateNode.RetractFindings();
            }
        }

        /// <summary>
        /// Resets the state histories for calculating next state and rewards
        /// </summary>
        /// <param name="domain">domain</param>
        /// <param name="history">history to reset to</param>
        /// <param name="sPrefix">state prefix</param>
        public static void ResetStateHistories(this Domain domain, List<uint> history, string sPrefix)
        {
            for (int i = 0; i < history.Count; i++)
            {
                DiscreteChanceNode stateNode = (DiscreteChanceNode)domain.GetNodeByName(sPrefix + (i + domain.GetFirstTimeStep()));
                stateNode.SelectState(history[i]);
            }
        }

        /// <summary>
        /// repairs cpts in model nodes
        /// </summary>
        /// <param name="domain"></param>
        /// <param name="modPrefix"></param>
        public static void RepairModelNodes(this Domain domain, string modPrefix)
        {
            for (int i = domain.GetFirstTimeStep() + 1; i <= domain.GetLastTimeStep(); i++)
            {
                Console.WriteLine(modPrefix + i);

                DiscreteChanceNode modelNode = (DiscreteChanceNode) domain.GetNodeByName(modPrefix + i);

                float[] table = modelNode.GetTable().GetData();
                uint[] combination = new uint[modelNode.GetParents().Count + 1];

                List<int> rowsWithoutSet = new List<int>();
                bool hasSet = false;
                int currentRow = 0;

                for (uint j = 0; j < table.Length; j++)
                {
                    table[j] = 0.0f;

                    modelNode.GetTable().GetConfiguration(ref combination, j);

                    string stateLabel = string.Empty;

                    for (int k = 0; k < modelNode.GetParents().Count; k++)
                    {
                        DiscreteChanceNode dn = (DiscreteChanceNode) modelNode.GetParents()[k];
                        stateLabel = stateLabel + "," + dn.GetStateLabel(combination[k]);
                    }

                    stateLabel = stateLabel.Substring(1); // remove starting comma

                    if (stateLabel.Equals(modelNode.GetStateLabel(combination[combination.Length - 1])))
                    {
                        table[j] = 1.0f;
                        hasSet = true;
                    }

                    if (combination[combination.Length-1] == (modelNode.GetNumberOfStates() - 1))
                    {
                        if (!hasSet)
                        {
                            rowsWithoutSet.Add(currentRow);
                        }

                        currentRow ++;
                        hasSet = false;
                    }
                }

                //prevent zero sum table error by setting rows with all 0's to all 1's
                foreach (int row in rowsWithoutSet)
                {
                    for (int j = 0; j < modelNode.GetNumberOfStates(); j++)
                    {
                        table[(row*modelNode.GetNumberOfStates()) + j] = 1.0f;
                    }
                }

                modelNode.GetTable().SetData(table);
            }
        }


        public static void RandomizeNodes(this Domain domain, Random rand, string obsPrefix, string statePrefix)
        {
            //randomize initial state
            DiscreteChanceNode firstStateNode = (DiscreteChanceNode)domain.GetNodeByName(statePrefix + domain.GetFirstTimeStep());
            firstStateNode.RandomizeInitialStateNode(rand);

            //randomize observations,
            //DiscreteChanceNode firstObservationNode = (DiscreteChanceNode)domain.GetNodeByName(obsPrefix + domain.GetFirstTimeStep());
            //firstObservationNode.RandomizeInitialObsNode(rand);

            //for (int i = (domain.GetFirstTimeStep() + 1); i <= domain.GetLastTimeStep(); i++)
            //{
            //    DiscreteChanceNode nextObservationNode = (DiscreteChanceNode)domain.GetNodeByName(obsPrefix + i);
            //    nextObservationNode.CopyCptFromInitialObsNode(firstObservationNode);
            //}
        }
    }
}
