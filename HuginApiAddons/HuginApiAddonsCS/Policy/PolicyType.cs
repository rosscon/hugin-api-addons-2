﻿namespace HuginApiAddonsCS.Policy
{
    public enum PolicyType
    {
        XML,
        CSV,
        TIKZ
    };
}
