﻿namespace HuginApiAddonsCS.Simulator
{
    public class SimulationResult
    {
        public string model_i { get; set; }
        public int timeStep { get; set; }
        public string state { get; set; }
        public string observation_i { get; set; }
        public string action_i { get; set; }
        public float util_i { get; set; }

        public string model_j { get; set; }
        public string observation_j { get; set; }
        public string action_j { get; set; }
        public float util_j { get; set; }

        public override string ToString()
        {
            return (timeStep + "," + state + "," + observation_i + "," + action_i + "," + util_i + "," + observation_j + "," + action_j + "," + util_j + "," + model_j + ",");
        }
    }
}
