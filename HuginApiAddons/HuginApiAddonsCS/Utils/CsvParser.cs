﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace HuginApiAddonsCS.Utils
{
    public class CsvParser
    {
        /// <summary>
        /// Gets column headings from first row in csv file
        /// </summary>
        /// <param name="inputFile">input file to get headdings from</param>
        /// <returns>list of column headings</returns>
        public List<string> GetColumnHeadings(string inputFile)
        {
            StreamReader sr = new StreamReader(inputFile);
            string line = sr.ReadLine();
            List<string> headings = line.Split(',').ToList();
            sr.Close();
            return headings;
        }

        /// <summary>
        /// Parses each row of csv file into a list of list of strings
        /// </summary>
        /// <param name="inputFile">input file</param>
        /// <param name="ignoreFirst">whether or not to ignore first row for headdings</param>
        /// <returns>list of parsed rows</returns>
        public List<List<string>> ParseRows(string inputFile, bool ignoreFirst)
        {
            StreamReader sr = new StreamReader(inputFile);
            List<List<string>> rows = new List<List<string>>();

            string line;

            if (ignoreFirst)
            {
                //reads first to ignore it
                line = sr.ReadLine();
            }

            while ((line = sr.ReadLine()) != null)
            {
                List<string> row = new List<string>();
                row = line.Split(',').ToList();
                rows.Add(row);
                //Thread.Sleep(1);
            }

            return rows;
        }
    }
}
