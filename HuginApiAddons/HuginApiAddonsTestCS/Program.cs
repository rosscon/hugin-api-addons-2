﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HuginApiAddonsTestCS.Programs;

namespace HuginApiAddonsTestCS
{
    class Program
    {
        private static Dictionary<string, IProgram> _programs = new Dictionary<string, IProgram>(); 

        static void Main(string[] args)
        {
            _programs.Add("DomainExpander", new DomainExpander());
            _programs.Add("PolicyGenerator", new PolicyTreeGenerator());
            _programs.Add("Simulator", new Simulator());
            _programs.Add("IdidExpandModels", new IdidExpandModels());
            _programs.Add("LearnTrees", new LearnTrees());
            _programs.Add("LearnTreesUnknown", new LearnTreeUnknown());
            _programs.Add("Randomizer", new Randomizer());

            if (args.Count() > 0)
            {
                string programName = args[0];
                _programs[programName].ExecuteProgram(args);
            }
            else
            {
                //Error Message and display options for each program
                Console.WriteLine("Error, must specify a program");
                foreach (var program in _programs)
                {
                    Console.WriteLine(program.Key);
                    string[] programOptions = program.Value.GetCommandOptions();

                    for (int i = 0; i < programOptions.Count(); i++)
                    {
                        Console.WriteLine("\t" + programOptions[i]);
                    }
                    Console.WriteLine(string.Empty);
                }

                Console.WriteLine("Press any key to continue ...");
                Console.ReadKey();
            }
        }
    }
}
