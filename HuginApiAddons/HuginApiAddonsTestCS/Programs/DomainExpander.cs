﻿using System;
using System.Linq;
using HuginApiAddonsCS;
using HuginApiAddonsCS.DomainExpander;

namespace HuginApiAddonsTestCS.Programs
{
    class DomainExpander : IProgram
    {
        #region IProgram Members

        public void ExecuteProgram(string[] args)
        {
            if (args.Count() == 5)
            {
                string inputNet = args[1];
                string outputNet = args[2];
                int numSteps = Convert.ToInt32(args[3]);
                DomainType dType = (DomainType)Enum.Parse(typeof(DomainType), args[4]);

                Expander expander = new Expander();
                expander.ExpandDomain(inputNet, outputNet, dType, numSteps);
            }
            else
            {
                Console.WriteLine("Invalid Parameters");
                Console.WriteLine("Press any key to continue ...");
                Console.ReadKey();
            }
        }

        public string[] GetCommandOptions()
        {
            string[] options =
            {
                "[input net] [output net] [number steps] [domain type]"
            };

            return options;
        }

        #endregion
    }
}
