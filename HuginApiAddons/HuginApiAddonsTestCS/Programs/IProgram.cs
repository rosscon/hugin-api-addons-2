﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HuginApiAddonsTestCS.Programs
{
    public interface IProgram
    {
        void ExecuteProgram(string[] args);
        string[] GetCommandOptions();
    }
}
