﻿using System;
using System.Collections.Generic;
using System.Linq;
using HuginApiAddonsCS.I_DID;

namespace HuginApiAddonsTestCS.Programs
{
    class IdidExpandModels : IProgram
    {
        #region IProgram Members

        public void ExecuteProgram(string[] args)
        {
            if (args.Count() >= 7)
            {
                PolicySource pSource = (PolicySource)Enum.Parse(typeof(PolicySource), args[1]);
                string ididNet = args[2];
                string outputNet = args[3];

                string ajPrefix = args[4];
                string modjPrefix = args[5];
                string ojPrefix = args[6];
                
                List<string> inputFiles = new List<string>();

                //Add input files to list of input files
                for (int i = 7; i < args.Count(); i++)
                {
                    inputFiles.Add(args[i]);
                }

                IdidEnterModels enterModels = new IdidEnterModels();

                switch (pSource)
                {
                    case PolicySource.NET:
                        enterModels.EnterNetModelsIntoIdid(ididNet, outputNet, inputFiles, ajPrefix, modjPrefix, ojPrefix);
                        break;

                    case PolicySource.CSV:
                        enterModels.EnterCsvModelsIntoIdid(ididNet, outputNet, inputFiles, ajPrefix, modjPrefix, ojPrefix);
                        break;

                    case PolicySource.XML:
                        enterModels.EnterXmlModelsIntoIdid(ididNet, outputNet, inputFiles, ajPrefix, modjPrefix, ojPrefix);
                        break;
                }
            }
            else
            {
                Console.WriteLine("Invalid Parameters");
                Console.WriteLine("Press any key to continue ...");
                Console.ReadKey();
            }
        }

        public string[] GetCommandOptions()
        {
            string[] options =
            {
                "[Did policy source (NET, CSV, XML)] [input I-DID net] [output I-DID net] [prefix aj] [prefix oj] [prefix mod] <input xml/csv/xml 1> ... <input xml/csv/xml n>"
            };

            return options;
        }

        #endregion
    }
}
