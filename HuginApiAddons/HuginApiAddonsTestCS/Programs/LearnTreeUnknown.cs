﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using HuginApiAddonsCS.I_DID;
using HuginApiAddonsCS.Policy;
using HuginApiAddonsCS.Policy.Learning;

namespace HuginApiAddonsTestCS.Programs
{
    public class LearnTreeUnknown: IProgram
    {
        #region IProgram Members

        public void ExecuteProgram(string[] args)
        {
            if (args.Count() > 7)
            {
                string obsCol = args[1];
                List<string> observations = args[2].Split(',').ToList();
                string actionCol = args[3];
                int maxHorizon = Convert.ToInt32(args[4]);
                string outputFolder = args[5];
                PolicyType pType = (PolicyType)Enum.Parse(typeof(PolicyType), args[6]);
                List<string> inputFiles = new List<string>();

                for (int i = 7; i < args.Count(); i++)
                {
                    inputFiles.Add(args[i]);
                }

                PolicyTreeLearner learner = new PolicyTreeLearner();
                List<PolicyNode> trees = learner.LearnFromUnknownHorizonHistories(inputFiles, obsCol, actionCol, observations, maxHorizon);

#if DEBUG
                int currentTree = 0;
                foreach (PolicyNode rootNode in trees)
                {
                    Console.Out.WriteLine(rootNode.ToTreeString(string.Empty, currentTree));
                    currentTree++;
                    switch (pType)
                    {
                        case PolicyType.TIKZ:
                            System.IO.StreamWriter file = new System.IO.StreamWriter(outputFolder + "\\" + currentTree + ".tex");
                            file.WriteLine(rootNode.ToTikzString());
                            file.Close();
                            break;
                        case PolicyType.XML:
                            XmlWriter xmlWriter = XmlWriter.Create(outputFolder + "\\" + currentTree + ".xml");
                            xmlWriter.WriteStartDocument();
                            rootNode.ToXmlWriter(xmlWriter);
                            xmlWriter.WriteEndDocument();
                            break;
                    }
                    
                }
#endif
            }
            else
            {
                Console.WriteLine("Invalid Parameters");
                Console.WriteLine("Press any key to continue ...");
                Console.ReadKey();
            }
        }

        public string[] GetCommandOptions()
        {
            string[] options =
            {
                "[input file] [obs col] [observations] [act col] [max horizon] [output folder] [output type] [input files]"
            };

            return options;
        }

        #endregion
    }
}
