﻿using System;
using System.Linq;
using HuginApiAddonsCS.Policy;
using HuginApiAddonsCS.Policy.PolicyGenerator;

namespace HuginApiAddonsTestCS.Programs
{
    class PolicyTreeGenerator : IProgram
    {
        #region IProgram Members

        public void ExecuteProgram(string[] args)
        {
            if (args.Count() == 6)
            {
                string inputNet = args[1];
                string actPrefix = args[2];
                string obsPrefix = args[3];
                PolicyType dType = (PolicyType)Enum.Parse(typeof(PolicyType), args[4]);
                string outputDir = args[5];

                PolicyGenerator generator = new PolicyGenerator();

                if (dType == PolicyType.XML)
                {
                    generator.GenerateXmlPolicy(inputNet, actPrefix, obsPrefix, outputDir);
                }
                else if(dType == PolicyType.CSV)
                {
                    generator.GenerateCsvPolicy(inputNet, actPrefix, obsPrefix, outputDir);
                }
                else if (dType == PolicyType.TIKZ)
                {
                    generator.GenerateTikzPolicy(inputNet, actPrefix, obsPrefix, outputDir);
                }
            }
            else
            {
                Console.WriteLine("Invalid Parameters");
                Console.WriteLine("Press any key to continue ...");
                Console.ReadKey();
            }
        }

        public string[] GetCommandOptions()
        {
            string[] options =
            {
                "[input net] [act prefix] [obs prefix] [output type (csv,xml)] [output dir/file]"
            };

            return options;
        }

        #endregion
    }
}
