﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HAPI;
using HuginApiAddonsCS.Extensions;

namespace HuginApiAddonsTestCS.Programs
{
    class Randomizer : IProgram
    {
        public void ExecuteProgram(string[] args)
        {
            if (args.Count() == 5)
            {
                string inputNet = args[1];
                string outputNet = args[2];
                string obsPrefix = args[3];
                string sPrefix = args[4];

                Random r = new Random(BitConverter.ToInt32(Guid.NewGuid().ToByteArray(), 0));
                
                ParseListener pl = new DefaultClassParseListener();
                Domain inDomain = new Domain(inputNet, pl);
                inDomain.RandomizeNodes(r, obsPrefix, sPrefix);
                inDomain.SaveAsNet(outputNet);
            }
        }

        public string[] GetCommandOptions()
        {
            throw new NotImplementedException();
        }
    }
}
