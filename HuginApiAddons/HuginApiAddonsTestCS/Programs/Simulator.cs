﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HuginApiAddonsCS;

namespace HuginApiAddonsTestCS.Programs
{
    class Simulator : IProgram
    {
        #region IProgram Members

        public void ExecuteProgram(string[] args)
        {
            if (args.Count() > 8)
            {
                HuginApiAddonsCS.Simulator.Simulator simulator = new HuginApiAddonsCS.Simulator.Simulator();
                int threadCount = Environment.ProcessorCount;
                Console.WriteLine("Thread Count: {0}", threadCount);

                DomainType dType = (DomainType)Enum.Parse(typeof(DomainType), args[1]);

                if (dType == DomainType.DID)
                {
                    string statePrefix = args[2];
                    string observationPrefix = args[3];
                    string actionPrefix = args[4];
                    string utilPrefix = args[5];
                    string outputFile = args[6];
                    int numberOfSims = Convert.ToInt32(args[7]);

                    List<string> inputFiles = new List<string>();

                    //Add input files to list of input files
                    for (int i = 8; i < args.Count(); i++)
                    {
                        inputFiles.Add(args[i]);
                    }
                    simulator.SimulateDid(statePrefix, observationPrefix, actionPrefix, utilPrefix, inputFiles, outputFile, numberOfSims, threadCount);
                }
                else if (dType == DomainType.IDID)
                {
                    string statePrefix = args[2];
                    string oiPrefix = args[3];
                    string aiPrefix = args[4];
                    string utilPrefix = args[5];
                    string ojPrefix = args[6];
                    string ajPrefix = args[7];
                    string outputFile = args[8];
                    int numberOfSims = Convert.ToInt32(args[9]);
                    string iDidFile = args[10];

                    List<string> inputFiles = new List<string>();

                    //Add input files to list of input files
                    for (int i = 11; i < args.Count(); i++)
                    {
                        inputFiles.Add(args[i]);
                    }
                    
                    simulator.SimulateIdid(statePrefix, oiPrefix, aiPrefix, utilPrefix, ojPrefix, ajPrefix, iDidFile, inputFiles, outputFile, numberOfSims, threadCount, -200, true);
                }
            }
            else
            {
                Console.WriteLine("Invalid Parameters");
                Console.WriteLine("Press any key to continue ...");
                Console.ReadKey();
            }
        }

        public string[] GetCommandOptions()
        {
            string[] options =
            {
                "DID [state] [obs] [act] [util] [results csv file] [num sims] [net file 1] ... [net file n]",
                "IDID [state] [obs i] [act i] [util] [obs j] [act j] [results csv file] [num sims] [IDID net file] [net file 1] ... [net file n]"
            };

            return options;
        }

        #endregion
    }
}
