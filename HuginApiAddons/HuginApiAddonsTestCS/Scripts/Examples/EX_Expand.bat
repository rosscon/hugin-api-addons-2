@echo off
COLOR 1F

cd ..\..\

TITLE SETUP DIRECTORIES
echo Setup Directories
mkdir out
del /Q out\*.*

TITLE EXPAND DID
echo Expand DID
HuginApiAddonsTestCS.exe DomainExpander TestNets\T_DID_3_M0.net TestNets\T_DID_3_M1.net 5 LIMID

TITLE EXPAND I-DID
echo Expand I-DID
HuginApiAddonsTestCS.exe DomainExpander TestNets\T_IDID_3.net out\TIDID8.net 5 LIMID


pause