@echo off
COLOR 1F

cd ..\..\

TITLE SETUP DIRECTORIES
echo Setup Directories
mkdir example_out
del /Q example_out\*.*
mkdir example_out\csv\

TITLE GENERATE POLICY
echo Generate Policy (XML)
HuginApiAddonsTestCS.exe PolicyGenerator nets\tigerDIDT3-0.net aj oj XML example_out\xml_policy.xml

echo Generate Policy (TIKZ)
HuginApiAddonsTestCS.exe PolicyGenerator nets\tigerDIDT3-0.net aj oj TIKZ example_out\tikz_policy.tex

REM CSV policy files are stored in 1 file per action node in diagram format
echo Generate Policy (CSV)
HuginApiAddonsTestCS.exe PolicyGenerator nets\tigerDIDT3-0.net aj oj CSV example_out\csv\



pause