@echo off
COLOR 1F

cd ..\..\

TITLE SETUP DIRECTORIES
echo Setup Directories
mkdir example_out
del /Q example_out\*.*

TITLE LEARN TREES
echo Learn Trees Known Horizon
HuginApiAddonsTestCS.exe LearnTrees example_out\t4\merged.csv Observation DontSee,SeeFar,SeeNear Action Model TimeStep

echo Learn Trees Unknown Horizon
mkdir example_out\unknown_horizon\
set inputDIR=C:\StarcraftReplayData\Extract\(al.ytong)(AL.normal)(Neutral)\
HuginApiAddonsTestCS.exe LearnTreesUnknown Observation DontSee,SeeFar,NeeNear Action 4 example_out\unknown_horizon\ TIKZ "%inputDIR%Protoss Dragoon_188.csv" "%inputDIR%Protoss Dragoon_197.csv" "%inputDIR%Protoss Dragoon_213.csv" "%inputDIR%Protoss Dragoon_224.csv" "%inputDIR%Protoss Dragoon_250.csv" "%inputDIR%Protoss Dragoon_263.csv" "%inputDIR%Protoss Dragoon_276.csv" "%inputDIR%Protoss Dragoon_280.csv" "%inputDIR%Protoss Dragoon_294.csv" "%inputDIR%Protoss Dragoon_297.csv" "%inputDIR%Protoss Dragoon_307.csv" "%inputDIR%Protoss Dragoon_313.csv" "%inputDIR%Protoss Dragoon_327.csv" "%inputDIR%Protoss Dragoon_346.csv"

pause