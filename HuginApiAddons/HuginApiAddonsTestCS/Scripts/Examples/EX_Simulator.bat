@echo off
COLOR 1F

cd ..\..\

TITLE SETUP DIRECTORIES
echo Setup Directories
mkdir out
del /Q out\*.*

TITLE DID SIMULATIONS
echo DID Simulations
HuginApiAddonsTestCS.exe Simulator DID s oj aj u out\did_simulations.csv 1000 TestNets\tigerDIDT4-0.net TestNets\tigerDIDT4-1.net

TITLE I-DID SIMULATIONS
echo DID Simulations
HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj out\TIDID3.csv 10000 TestNets\T_IDID_3.net TestNets\T_DID_3_M0.net TestNets\T_DID_3_M0.net




pause