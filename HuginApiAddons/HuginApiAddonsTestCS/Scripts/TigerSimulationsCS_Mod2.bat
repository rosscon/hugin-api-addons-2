@echo off
COLOR 0A

TITLE CLEAR RESULTS FOLDER
del /Q ..\simulations\*.*

TITLE EXPAND DOMAINS
echo Expand Domains
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID4.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID6.net 3 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT4-0.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT4-1.net 1 LIMID
REM ..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT4-2.net 1 LIMID
REM ..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT4-3.net 1 LIMID
REM ..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT4-4.net 1 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT6-0.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT6-1.net 3 LIMID
REM ..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT6-2.net 3 LIMID
REM ..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT6-3.net 3 LIMID
REM ..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT6-4.net 3 LIMID

echo DID Simulations

TITLE DID SIMULATIONS T=4
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_10.csv 10 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
Copy ..\simulations\tigerT4M2_10.csv ..\simulations\tigerT4M2_20.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_20.csv 20 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
Copy ..\simulations\tigerT4M2_20.csv ..\simulations\tigerT4M2_50.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_50.csv 30 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
Copy ..\simulations\tigerT4M2_50.csv ..\simulations\tigerT4M2_100.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_100.csv 50 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
Copy ..\simulations\tigerT4M2_100.csv ..\simulations\tigerT4M2_200.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_200.csv 100 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
Copy ..\simulations\tigerT4M2_200.csv ..\simulations\tigerT4M2_500.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_500.csv 300 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
Copy ..\simulations\tigerT4M2_500.csv ..\simulations\tigerT4M2_1000.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_1000.csv 500 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
Copy ..\simulations\tigerT4M2_1000.csv ..\simulations\tigerT4M2_2000.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M2_2000.csv 1000 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

TITLE DID SIMULATIONS T=6
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_10.csv 10 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
Copy ..\simulations\tigerT6M2_10.csv ..\simulations\tigerT6M2_20.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_20.csv 10 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
Copy ..\simulations\tigerT6M2_20.csv ..\simulations\tigerT6M2_50.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_50.csv 30 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
Copy ..\simulations\tigerT6M2_50.csv ..\simulations\tigerT6M2_100.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_100.csv 50 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
Copy ..\simulations\tigerT6M2_100.csv ..\simulations\tigerT6M2_200.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_200.csv 100 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
Copy ..\simulations\tigerT6M2_200.csv ..\simulations\tigerT6M2_500.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_500.csv 300 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
Copy ..\simulations\tigerT6M2_500.csv ..\simulations\tigerT6M2_1000.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_1000.csv 500 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
Copy ..\simulations\tigerT6M2_1000.csv ..\simulations\tigerT6M2_2000.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M2_2000.csv 1000 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

TITLE Learning DID Policies T=4
set epsilon=0.3
echo T4 10
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_10.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_10_Rand.net aj oj mod ..\nets\TIDID4_10_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_10_Merges.csv
echo T4 20
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_20.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_20_Rand.net aj oj mod ..\nets\TIDID4_20_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_20_Merges.csv
echo T4 50
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_50.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_50_Rand.net aj oj mod ..\nets\TIDID4_50_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_50_Merges.csv
echo T4 100
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_100.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_100_Rand.net aj oj mod ..\nets\TIDID4_100_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_100_Merges.csv
echo T4 200
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_200.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_200_Rand.net aj oj mod ..\nets\TIDID4_200_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_200_Merges.csv
echo T4 500
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_500.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_500_Rand.net aj oj mod ..\nets\TIDID4_500_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_500_Merges.csv
echo T4 1000
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_1000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_1000_Rand.net aj oj mod ..\nets\TIDID4_1000_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_1000_Merges.csv
echo T4 2000
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M2_2000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_2000_Rand.net aj oj mod ..\nets\TIDID4_2000_BCT.net SIM %epsilon% ..\simulations\tigerT4M2_2000_Merges.csv

TITLE Learning DID Policies T=6
echo T6 10
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_10.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_10_Rand.net aj oj mod ..\nets\TIDID6_10_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_10_Merges.csv
echo T6 20
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_20.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_20_Rand.net aj oj mod ..\nets\TIDID6_20_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_20_Merges.csv
echo T6 50
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_50.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_50_Rand.net aj oj mod ..\nets\TIDID6_50_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_50_Merges.csv
echo T6 100
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_100.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_100_Rand.net aj oj mod ..\nets\TIDID6_100_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_100_Merges.csv
echo T6 200
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_200.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_200_Rand.net aj oj mod ..\nets\TIDID6_200_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_200_Merges.csv
echo T6 500
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_500.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_500_Rand.net aj oj mod ..\nets\TIDID6_500_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_500_Merges.csv
echo T6 1000
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_1000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_1000_Rand.net aj oj mod ..\nets\TIDID6_1000_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_1000_Merges.csv
echo T6 2000
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M2_2000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_2000_Rand.net aj oj mod ..\nets\TIDID6_2000_BCT.net SIM %epsilon% ..\simulations\tigerT6M2_2000_Merges.csv


TITLE I-DID Simulations T=4
set numsims=5000
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_10_Rand.csv %numSims% ..\nets\TIDID4_10_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_10_BCT.csv %numSims% ..\nets\TIDID4_10_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_20_Rand.csv %numSims% ..\nets\TIDID4_20_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_20_BCT.csv %numSims% ..\nets\TIDID4_20_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_50_Rand.csv %numSims% ..\nets\TIDID4_50_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_50_BCT.csv %numSims% ..\nets\TIDID4_50_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_100_Rand.csv %numSims% ..\nets\TIDID4_100_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_100_BCT.csv %numSims% ..\nets\TIDID4_100_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_500_Rand.csv %numSims% ..\nets\TIDID4_500_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_500_BCT.csv %numSims% ..\nets\TIDID4_500_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_1000_Rand.csv %numSims% ..\nets\TIDID4_1000_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_1000_BCT.csv %numSims% ..\nets\TIDID4_1000_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_2000_Rand.csv %numSims% ..\nets\TIDID4_2000_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_2000_BCT.csv %numSims% ..\nets\TIDID4_2000_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net

TITLE I-DID Simulations T=6
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_10_Rand.csv %numSims% ..\nets\TIDID6_10_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_10_BCT.csv %numSims% ..\nets\TIDID6_10_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_20_Rand.csv %numSims% ..\nets\TIDID6_20_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_20_BCT.csv %numSims% ..\nets\TIDID6_20_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_50_Rand.csv %numSims% ..\nets\TIDID6_50_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_50_BCT.csv %numSims% ..\nets\TIDID6_50_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_100_Rand.csv %numSims% ..\nets\TIDID6_100_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_100_BCT.csv %numSims% ..\nets\TIDID6_100_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_500_Rand.csv %numSims% ..\nets\TIDID6_500_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_500_BCT.csv %numSims% ..\nets\TIDID6_500_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_1000_Rand.csv %numSims% ..\nets\TIDID6_1000_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_1000_BCT.csv %numSims% ..\nets\TIDID6_1000_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_2000_Rand.csv %numSims% ..\nets\TIDID6_2000_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_2000_BCT.csv %numSims% ..\nets\TIDID6_2000_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

TITLE Complete

pause