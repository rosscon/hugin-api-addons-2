@echo off
COLOR 0A

TITLE CLEAR RESULTS FOLDER
mkdir ..\simulations\
del /Q ..\simulations\*.*

TITLE EXPAND DOMAINS
echo Expand Domains
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID4.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID6.net 3 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT4-0.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT4-1.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT4-2.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT4-3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT4-4.net 1 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT6-0.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT6-1.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT6-2.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT6-3.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT6-4.net 3 LIMID

echo DID Simulations

TITLE DID SIMULATIONS T=4
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_02.csv 2 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
Copy ..\simulations\tigerT4M5_02.csv ..\simulations\tigerT4M5_05.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_05.csv 3 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
Copy ..\simulations\tigerT4M5_05.csv ..\simulations\tigerT4M5_10.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_10.csv 5 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
Copy ..\simulations\tigerT4M5_10.csv ..\simulations\tigerT4M5_20.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_20.csv 20 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
Copy ..\simulations\tigerT4M5_20.csv ..\simulations\tigerT4M5_50.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_50.csv 30 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
Copy ..\simulations\tigerT4M5_50.csv ..\simulations\tigerT4M5_100.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_100.csv 50 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
REM Copy ..\simulations\tigerT4M5_100.csv ..\simulations\tigerT4M5_200.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_200.csv 100 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
REM Copy ..\simulations\tigerT4M5_200.csv ..\simulations\tigerT4M5_500.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_500.csv 300 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
REM Copy ..\simulations\tigerT4M5_500.csv ..\simulations\tigerT4M5_1000.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_1000.csv 500 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
REM Copy ..\simulations\tigerT4M5_1000.csv ..\simulations\tigerT4M5_2000.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_2000.csv 1000 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net

TITLE DID SIMULATIONS T=6
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_02.csv 2 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
Copy ..\simulations\tigerT6M5_02.csv ..\simulations\tigerT6M5_05.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_05.csv 3 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
Copy ..\simulations\tigerT6M5_05.csv ..\simulations\tigerT6M5_10.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_10.csv 5 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
Copy ..\simulations\tigerT6M5_10.csv ..\simulations\tigerT6M5_20.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_20.csv 20 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
Copy ..\simulations\tigerT6M5_20.csv ..\simulations\tigerT6M5_50.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_50.csv 30 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
Copy ..\simulations\tigerT6M5_50.csv ..\simulations\tigerT6M5_100.csv
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_100.csv 50 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
REM Copy ..\simulations\tigerT6M5_100.csv ..\simulations\tigerT6M5_200.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_200.csv 100 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
REM Copy ..\simulations\tigerT6M5_200.csv ..\simulations\tigerT6M5_500.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_500.csv 300 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
REM Copy ..\simulations\tigerT6M5_500.csv ..\simulations\tigerT6M5_1000.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_1000.csv 500 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
REM Copy ..\simulations\tigerT6M5_1000.csv ..\simulations\tigerT6M5_2000.csv
REM ..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_2000.csv 1000 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net

TITLE Learning DID Policies T=4
set epsilon=0.35
echo T4 2
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_02.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_02_Rand.net aj oj mod ..\nets\TIDID4_02_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_02_Merges.csv
echo T4 5
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_05.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_05_Rand.net aj oj mod ..\nets\TIDID4_05_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_05_Merges.csv
echo T4 10
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_10.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_10_Rand.net aj oj mod ..\nets\TIDID4_10_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_10_Merges.csv
echo T4 20
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_20.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_20_Rand.net aj oj mod ..\nets\TIDID4_20_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_20_Merges.csv
echo T4 50
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_50.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_50_Rand.net aj oj mod ..\nets\TIDID4_50_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_50_Merges.csv
echo T4 100
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_100.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_100_Rand.net aj oj mod ..\nets\TIDID4_100_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_100_Merges.csv
REM echo T4 200
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_200.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_200_Rand.net aj oj mod ..\nets\TIDID4_200_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_200_Merges.csv
REM echo T4 500
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_500.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_500_Rand.net aj oj mod ..\nets\TIDID4_500_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_500_Merges.csv
REM echo T4 1000
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_1000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_1000_Rand.net aj oj mod ..\nets\TIDID4_1000_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_1000_Merges.csv
REM echo T4 2000
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT4M5_2000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID4.net ..\nets\TIDID4_2000_Rand.net aj oj mod ..\nets\TIDID4_2000_BCT.net SIM %epsilon% ..\simulations\tigerT4M5_2000_Merges.csv

TITLE Learning DID Policies T=6
echo T6 2
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_02.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_02_Rand.net aj oj mod ..\nets\TIDID6_02_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_02_Merges.csv
echo T6 5
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_05.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_05_Rand.net aj oj mod ..\nets\TIDID6_05_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_05_Merges.csv
echo T6 10
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_10.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_10_Rand.net aj oj mod ..\nets\TIDID6_10_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_10_Merges.csv
echo T6 20
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_20.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_20_Rand.net aj oj mod ..\nets\TIDID6_20_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_20_Merges.csv
echo T6 50
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_50.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_50_Rand.net aj oj mod ..\nets\TIDID6_50_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_50_Merges.csv
echo T6 100
..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_100.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_100_Rand.net aj oj mod ..\nets\TIDID6_100_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_100_Merges.csv
REM echo T6 200
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_200.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_200_Rand.net aj oj mod ..\nets\TIDID6_200_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_200_Merges.csv
REM echo T6 500
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_500.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_500_Rand.net aj oj mod ..\nets\TIDID6_500_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_500_Merges.csv
REM echo T6 1000
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_1000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_1000_Rand.net aj oj mod ..\nets\TIDID6_1000_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_1000_Merges.csv
REM echo T6 2000
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\simulations\tigerT6M5_2000.csv Observation GL,GR Action Model TimeStep ..\nets\TIDID6.net ..\nets\TIDID6_2000_Rand.net aj oj mod ..\nets\TIDID6_2000_BCT.net SIM %epsilon% ..\simulations\tigerT6M5_2000_Merges.csv


TITLE I-DID Simulations T=4
set numsims=5000
echo T4 2
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_02_Rand.csv %numSims% ..\nets\TIDID4_02_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_02_BCT.csv %numSims% ..\nets\TIDID4_02_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
echo T4 5
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_05_Rand.csv %numSims% ..\nets\TIDID4_05_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_05_BCT.csv %numSims% ..\nets\TIDID4_05_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
echo T4 10
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_10_Rand.csv %numSims% ..\nets\TIDID4_10_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_10_BCT.csv %numSims% ..\nets\TIDID4_10_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
echo T4 20
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_20_Rand.csv %numSims% ..\nets\TIDID4_20_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_20_BCT.csv %numSims% ..\nets\TIDID4_20_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
echo T4 50
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_50_Rand.csv %numSims% ..\nets\TIDID4_50_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_50_BCT.csv %numSims% ..\nets\TIDID4_50_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
echo T4 100
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_100_Rand.csv %numSims% ..\nets\TIDID4_100_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_100_BCT.csv %numSims% ..\nets\TIDID4_100_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net

REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_500_Rand.csv %numSims% ..\nets\TIDID4_500_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_500_BCT.csv %numSims% ..\nets\TIDID4_500_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net

REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_1000_Rand.csv %numSims% ..\nets\TIDID4_1000_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_1000_BCT.csv %numSims% ..\nets\TIDID4_1000_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net

REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_2000_Rand.csv %numSims% ..\nets\TIDID4_2000_Rand.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID4_2000_BCT.csv %numSims% ..\nets\TIDID4_2000_BCT.net ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net

TITLE I-DID Simulations T=6
echo T6 2
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_02_Rand.csv %numSims% ..\nets\TIDID6_02_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_02_BCT.csv %numSims% ..\nets\TIDID6_02_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
echo T6 5
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_05_Rand.csv %numSims% ..\nets\TIDID6_05_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_05_BCT.csv %numSims% ..\nets\TIDID6_05_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
echo T6 10
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_10_Rand.csv %numSims% ..\nets\TIDID6_10_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_10_BCT.csv %numSims% ..\nets\TIDID6_10_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
echo T6 20
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_20_Rand.csv %numSims% ..\nets\TIDID6_20_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_20_BCT.csv %numSims% ..\nets\TIDID6_20_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
echo T6 50
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_50_Rand.csv %numSims% ..\nets\TIDID6_50_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_50_BCT.csv %numSims% ..\nets\TIDID6_50_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
echo T6 100
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_100_Rand.csv %numSims% ..\nets\TIDID6_100_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_100_BCT.csv %numSims% ..\nets\TIDID6_100_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net

REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_500_Rand.csv %numSims% ..\nets\TIDID6_500_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_500_BCT.csv %numSims% ..\nets\TIDID6_500_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net

REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_1000_Rand.csv %numSims% ..\nets\TIDID6_1000_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_1000_BCT.csv %numSims% ..\nets\TIDID6_1000_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net

REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_2000_Rand.csv %numSims% ..\nets\TIDID6_2000_Rand.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
REM ..\HuginApiAddonsTestCS.exe Simulator IDID s oi ai u oj aj ..\simulations\TIDID6_2000_BCT.csv %numSims% ..\nets\TIDID6_2000_BCT.net ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net

TITLE Complete

pause