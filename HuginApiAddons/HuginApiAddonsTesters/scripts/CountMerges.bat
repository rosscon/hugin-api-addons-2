@echo off
set epsilon=1.5

echo Learn and fill trees from 15 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT3-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\01_TIDID3_Partial_15.net [aj,mod] 3 ..\nets\01_TIDID3_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT3-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\02_TIDID3_Partial_15.net [aj,mod] 3 ..\nets\02_TIDID3_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT3-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\03_TIDID3_Partial_15.net [aj,mod] 3 ..\nets\03_TIDID3_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT3-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\04_TIDID3_Partial_15.net [aj,mod] 3 ..\nets\04_TIDID3_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT3-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\05_TIDID3_Partial_15.net [aj,mod] 3 ..\nets\05_TIDID3_Filled_15.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT4-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\01_TIDID4_Partial_15.net [aj,mod] 4 ..\nets\01_TIDID4_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT4-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\02_TIDID4_Partial_15.net [aj,mod] 4 ..\nets\02_TIDID4_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT4-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\03_TIDID4_Partial_15.net [aj,mod] 4 ..\nets\03_TIDID4_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT4-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\04_TIDID4_Partial_15.net [aj,mod] 4 ..\nets\04_TIDID4_Filled_15.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT4-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\05_TIDID4_Partial_15.net [aj,mod] 4 ..\nets\05_TIDID4_Filled_15.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT5-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\01_TIDID5_Partial_15.net [aj,mod] 5 ..\nets\01_TIDID5_Filled_15.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT5-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\02_TIDID5_Partial_15.net [aj,mod] 5 ..\nets\02_TIDID5_Filled_15.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT5-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\03_TIDID5_Partial_15.net [aj,mod] 5 ..\nets\03_TIDID5_Filled_15.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT5-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\04_TIDID5_Partial_15.net [aj,mod] 5 ..\nets\04_TIDID5_Filled_15.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT5-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\05_TIDID5_Partial_15.net [aj,mod] 5 ..\nets\05_TIDID5_Filled_15.net %epsilon%

echo Learn and fill trees from 30 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT3-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\01_TIDID3_Partial_30.net [aj,mod] 3 ..\nets\01_TIDID3_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT3-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\02_TIDID3_Partial_30.net [aj,mod] 3 ..\nets\02_TIDID3_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT3-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\03_TIDID3_Partial_30.net [aj,mod] 3 ..\nets\03_TIDID3_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT3-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\04_TIDID3_Partial_30.net [aj,mod] 3 ..\nets\04_TIDID3_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT3-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\05_TIDID3_Partial_30.net [aj,mod] 3 ..\nets\05_TIDID3_Filled_30.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT4-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\01_TIDID4_Partial_30.net [aj,mod] 4 ..\nets\01_TIDID4_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT4-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\02_TIDID4_Partial_30.net [aj,mod] 4 ..\nets\02_TIDID4_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT4-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\03_TIDID4_Partial_30.net [aj,mod] 4 ..\nets\03_TIDID4_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT4-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\04_TIDID4_Partial_30.net [aj,mod] 4 ..\nets\04_TIDID4_Filled_30.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT4-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\05_TIDID4_Partial_30.net [aj,mod] 4 ..\nets\05_TIDID4_Filled_30.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT5-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\01_TIDID5_Partial_30.net [aj,mod] 5 ..\nets\01_TIDID5_Filled_30.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT5-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\02_TIDID5_Partial_30.net [aj,mod] 5 ..\nets\02_TIDID5_Filled_30.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT5-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\03_TIDID5_Partial_30.net [aj,mod] 5 ..\nets\03_TIDID5_Filled_30.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT5-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\04_TIDID5_Partial_30.net [aj,mod] 5 ..\nets\04_TIDID5_Filled_30.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT5-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\05_TIDID5_Partial_30.net [aj,mod] 5 ..\nets\05_TIDID5_Filled_30.net %epsilon%

echo Learn and fill trees from 45 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT3-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\01_TIDID3_Partial_45.net [aj,mod] 3 ..\nets\01_TIDID3_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT3-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\02_TIDID3_Partial_45.net [aj,mod] 3 ..\nets\02_TIDID3_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT3-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\03_TIDID3_Partial_45.net [aj,mod] 3 ..\nets\03_TIDID3_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT3-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\04_TIDID3_Partial_45.net [aj,mod] 3 ..\nets\04_TIDID3_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT3-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\05_TIDID3_Partial_45.net [aj,mod] 3 ..\nets\05_TIDID3_Filled_45.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT4-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\01_TIDID4_Partial_45.net [aj,mod] 4 ..\nets\01_TIDID4_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT4-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\02_TIDID4_Partial_45.net [aj,mod] 4 ..\nets\02_TIDID4_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT4-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\03_TIDID4_Partial_45.net [aj,mod] 4 ..\nets\03_TIDID4_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT4-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\04_TIDID4_Partial_45.net [aj,mod] 4 ..\nets\04_TIDID4_Filled_45.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT4-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\05_TIDID4_Partial_45.net [aj,mod] 4 ..\nets\05_TIDID4_Filled_45.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT5-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\01_TIDID5_Partial_45.net [aj,mod] 5 ..\nets\01_TIDID5_Filled_45.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT5-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\02_TIDID5_Partial_45.net [aj,mod] 5 ..\nets\02_TIDID5_Filled_45.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT5-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\03_TIDID5_Partial_45.net [aj,mod] 5 ..\nets\03_TIDID5_Filled_45.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT5-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\04_TIDID5_Partial_45.net [aj,mod] 5 ..\nets\04_TIDID5_Filled_45.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT5-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\05_TIDID5_Partial_45.net [aj,mod] 5 ..\nets\05_TIDID5_Filled_45.net %epsilon%

echo Learn and fill trees from 60 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT3-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\01_TIDID3_Partial_60.net [aj,mod] 3 ..\nets\01_TIDID3_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT3-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\02_TIDID3_Partial_60.net [aj,mod] 3 ..\nets\02_TIDID3_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT3-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\03_TIDID3_Partial_60.net [aj,mod] 3 ..\nets\03_TIDID3_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT3-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\04_TIDID3_Partial_60.net [aj,mod] 3 ..\nets\04_TIDID3_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT3-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\05_TIDID3_Partial_60.net [aj,mod] 3 ..\nets\05_TIDID3_Filled_60.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT4-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\01_TIDID4_Partial_60.net [aj,mod] 4 ..\nets\01_TIDID4_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT4-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\02_TIDID4_Partial_60.net [aj,mod] 4 ..\nets\02_TIDID4_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT4-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\03_TIDID4_Partial_60.net [aj,mod] 4 ..\nets\03_TIDID4_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT4-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\04_TIDID4_Partial_60.net [aj,mod] 4 ..\nets\04_TIDID4_Filled_60.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT4-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\05_TIDID4_Partial_60.net [aj,mod] 4 ..\nets\05_TIDID4_Filled_60.net %epsilon%

echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT5-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\01_TIDID5_Partial_60.net [aj,mod] 5 ..\nets\01_TIDID5_Filled_60.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT5-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\02_TIDID5_Partial_60.net [aj,mod] 5 ..\nets\02_TIDID5_Filled_60.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT5-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\03_TIDID5_Partial_60.net [aj,mod] 5 ..\nets\03_TIDID5_Filled_60.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT5-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\04_TIDID5_Partial_60.net [aj,mod] 5 ..\nets\04_TIDID5_Filled_60.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT5-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\05_TIDID5_Partial_60.net [aj,mod] 5 ..\nets\05_TIDID5_Filled_60.net %epsilon%

echo Learn and fill trees from 75 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT3-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\01_TIDID3_Partial_75.net [aj,mod] 3 ..\nets\01_TIDID3_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT3-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\02_TIDID3_Partial_75.net [aj,mod] 3 ..\nets\02_TIDID3_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT3-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\03_TIDID3_Partial_75.net [aj,mod] 3 ..\nets\03_TIDID3_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT3-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\04_TIDID3_Partial_75.net [aj,mod] 3 ..\nets\04_TIDID3_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT3-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\05_TIDID3_Partial_75.net [aj,mod] 3 ..\nets\05_TIDID3_Filled_75.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT4-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\01_TIDID4_Partial_75.net [aj,mod] 4 ..\nets\01_TIDID4_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT4-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\02_TIDID4_Partial_75.net [aj,mod] 4 ..\nets\02_TIDID4_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT4-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\03_TIDID4_Partial_75.net [aj,mod] 4 ..\nets\03_TIDID4_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT4-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\04_TIDID4_Partial_75.net [aj,mod] 4 ..\nets\04_TIDID4_Filled_75.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT4-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\05_TIDID4_Partial_75.net [aj,mod] 4 ..\nets\05_TIDID4_Filled_75.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT5-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\01_TIDID5_Partial_75.net [aj,mod] 5 ..\nets\01_TIDID5_Filled_75.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT5-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\02_TIDID5_Partial_75.net [aj,mod] 5 ..\nets\02_TIDID5_Filled_75.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT5-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\03_TIDID5_Partial_75.net [aj,mod] 5 ..\nets\03_TIDID5_Filled_75.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT5-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\04_TIDID5_Partial_75.net [aj,mod] 5 ..\nets\04_TIDID5_Filled_75.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT5-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\05_TIDID5_Partial_75.net [aj,mod] 5 ..\nets\05_TIDID5_Filled_75.net %epsilon%

echo Learn and fill trees from 90 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT3-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\01_TIDID3_Partial_90.net [aj,mod] 3 ..\nets\01_TIDID3_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT3-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\02_TIDID3_Partial_90.net [aj,mod] 3 ..\nets\02_TIDID3_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT3-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\03_TIDID3_Partial_90.net [aj,mod] 3 ..\nets\03_TIDID3_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT3-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\04_TIDID3_Partial_90.net [aj,mod] 3 ..\nets\04_TIDID3_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT3-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\05_TIDID3_Partial_90.net [aj,mod] 3 ..\nets\05_TIDID3_Filled_90.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT4-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\01_TIDID4_Partial_90.net [aj,mod] 4 ..\nets\01_TIDID4_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT4-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\02_TIDID4_Partial_90.net [aj,mod] 4 ..\nets\02_TIDID4_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT4-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\03_TIDID4_Partial_90.net [aj,mod] 4 ..\nets\03_TIDID4_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT4-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\04_TIDID4_Partial_90.net [aj,mod] 4 ..\nets\04_TIDID4_Filled_90.net %epsilon%
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT4-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\05_TIDID4_Partial_90.net [aj,mod] 4 ..\nets\05_TIDID4_Filled_90.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\01_DIDsimulationsT5-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\01_TIDID5_Partial_90.net [aj,mod] 5 ..\nets\01_TIDID5_Filled_90.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\02_DIDsimulationsT5-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\02_TIDID5_Partial_90.net [aj,mod] 5 ..\nets\02_TIDID5_Filled_90.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\03_DIDsimulationsT5-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\03_TIDID5_Partial_90.net [aj,mod] 5 ..\nets\03_TIDID5_Filled_90.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\04_DIDsimulationsT5-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\04_TIDID5_Partial_90.net [aj,mod] 5 ..\nets\04_TIDID5_Filled_90.net %epsilon%
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\output\05_DIDsimulationsT5-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\05_TIDID5_Partial_90.net [aj,mod] 5 ..\nets\05_TIDID5_Filled_90.net %epsilon%


echo: 
echo: 

REM I-DID_Simulations.bat

pause