@echo off
set t3models=..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT3-5.net ..\nets\tigerDIDT3-6.net ..\nets\tigerDIDT3-7.net ..\nets\tigerDIDT3-8.net ..\nets\tigerDIDT3-9.net 
REM ..\nets\tigerDIDT3-10.net ..\nets\tigerDIDT3-11.net ..\nets\tigerDIDT3-12.net ..\nets\tigerDIDT3-13.net ..\nets\tigerDIDT3-14.net 
REM ..\nets\tigerDIDT3-15.net ..\nets\tigerDIDT3-16.net ..\nets\tigerDIDT3-17.net ..\nets\tigerDIDT3-18.net ..\nets\tigerDIDT3-19.net

set t4models=..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net ..\nets\tigerDIDT4-5.net ..\nets\tigerDIDT4-6.net ..\nets\tigerDIDT4-7.net ..\nets\tigerDIDT4-8.net ..\nets\tigerDIDT4-9.net 
REM ..\nets\tigerDIDT4-10.net ..\nets\tigerDIDT4-11.net ..\nets\tigerDIDT4-12.net ..\nets\tigerDIDT4-13.net ..\nets\tigerDIDT4-14.net 
REM ..\nets\tigerDIDT4-15.net ..\nets\tigerDIDT4-16.net ..\nets\tigerDIDT4-17.net ..\nets\tigerDIDT4-18.net ..\nets\tigerDIDT4-19.net

set t5models=..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net 
REM ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net

set t5models=..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net 
REM ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net

echo Exact Policies
echo T = 3
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Exact.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID3_DID_Models.net %t3models%
echo T = 4
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Exact.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID4_DID_Models.net %t4models%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Exact.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_DID_Models.net %t5models%