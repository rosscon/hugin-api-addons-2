@echo off
echo SetupDirectories
mkdir ..\PolicyFiles\DIDM0T3\
mkdir ..\PolicyFiles\DIDM1T3\
mkdir ..\PolicyFiles\DIDM2T3\
mkdir ..\PolicyFiles\DIDM3T3\
mkdir ..\PolicyFiles\DIDM4T3\
REM mkdir ..\PolicyFiles\DIDM5T3\
REM mkdir ..\PolicyFiles\DIDM6T3\
REM mkdir ..\PolicyFiles\DIDM7T3\
REM mkdir ..\PolicyFiles\DIDM8T3\
REM mkdir ..\PolicyFiles\DIDM9T3\

mkdir ..\PolicyFiles\DIDM0T4\
mkdir ..\PolicyFiles\DIDM1T4\
mkdir ..\PolicyFiles\DIDM2T4\
mkdir ..\PolicyFiles\DIDM3T4\
mkdir ..\PolicyFiles\DIDM4T4\
REM mkdir ..\PolicyFiles\DIDM5T4\
REM mkdir ..\PolicyFiles\DIDM6T4\
REM mkdir ..\PolicyFiles\DIDM7T4\
REM mkdir ..\PolicyFiles\DIDM8T4\
REM mkdir ..\PolicyFiles\DIDM9T4\

mkdir ..\PolicyFiles\IDIDT3_2\
mkdir ..\PolicyFiles\IDIDT4_2\
mkdir ..\PolicyFiles\IDIDT3_5\
mkdir ..\PolicyFiles\IDIDT4_5\
REM mkdir ..\PolicyFiles\IDIDT3_10\
REM mkdir ..\PolicyFiles\IDIDT4_10\
mkdir ..\nets\StarCraft\

echo Expand Models
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID0_NewModel.net ..\nets\StarCraft\StarCraftDID0T3_NewModel.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID1_NewModel.net ..\nets\StarCraft\StarCraftDID1T3_NewModel.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID2_NewModel.net ..\nets\StarCraft\StarCraftDID2T3_NewModel.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID3_NewModel.net ..\nets\StarCraft\StarCraftDID3T3_NewModel.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID4_NewModel.net ..\nets\StarCraft\StarCraftDID4T3_NewModel.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID5_NewModel.net ..\nets\StarCraft\StarCraftDID5T3_NewModel.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID6_NewModel.net ..\nets\StarCraft\StarCraftDID6T3_NewModel.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID7_NewModel.net ..\nets\StarCraft\StarCraftDID7T3_NewModel.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID8_NewModel.net ..\nets\StarCraft\StarCraftDID8T3_NewModel.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID9_NewModel.net ..\nets\StarCraft\StarCraftDID9T3_NewModel.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID0.net ..\nets\StarCraft\StarCraftDID0T3.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID1.net ..\nets\StarCraft\StarCraftDID1T3.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID2.net ..\nets\StarCraft\StarCraftDID2T3.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID3.net ..\nets\StarCraft\StarCraftDID3T3.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID4.net ..\nets\StarCraft\StarCraftDID4T3.net LIMID 1

..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID0_NewModel.net ..\nets\StarCraft\StarCraftDID0T4_NewModel.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID1_NewModel.net ..\nets\StarCraft\StarCraftDID1T4_NewModel.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID2_NewModel.net ..\nets\StarCraft\StarCraftDID2T4_NewModel.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID3_NewModel.net ..\nets\StarCraft\StarCraftDID3T4_NewModel.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID4_NewModel.net ..\nets\StarCraft\StarCraftDID4T4_NewModel.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID5_NewModel.net ..\nets\StarCraft\StarCraftDID5T4_NewModel.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID6_NewModel.net ..\nets\StarCraft\StarCraftDID6T4_NewModel.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID7_NewModel.net ..\nets\StarCraft\StarCraftDID7T4_NewModel.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID8_NewModel.net ..\nets\StarCraft\StarCraftDID8T4_NewModel.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID9_NewModel.net ..\nets\StarCraft\StarCraftDID9T4_NewModel.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID0.net ..\nets\StarCraft\StarCraftDID0T4.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID1.net ..\nets\StarCraft\StarCraftDID1T4.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID2.net ..\nets\StarCraft\StarCraftDID2T4.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID3.net ..\nets\StarCraft\StarCraftDID3T4.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID4.net ..\nets\StarCraft\StarCraftDID4T4.net LIMID 2

..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftIDID_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftIDID_NewModel.net ..\nets\StarCraft\StarCraftIDIDT4_NewModel.net LIMID 2

REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftIDID.net ..\nets\StarCraft\StarCraftIDIDT3.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftIDID.net ..\nets\StarCraft\StarCraftIDIDT4.net LIMID 2

echo Enter DID Models Into I-DID
..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_2_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T3_NewModel.net ..\nets\StarCraft\StarCraftDID1T3_NewModel.net
..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT4_NewModel.net ..\nets\StarCraft\StarCraftIDIDT4_2_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T4_NewModel.net ..\nets\StarCraft\StarCraftDID1T4_NewModel.net

..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_5_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T3_NewModel.net ..\nets\StarCraft\StarCraftDID1T3_NewModel.net ..\nets\StarCraft\StarCraftDID2T3_NewModel.net ..\nets\StarCraft\StarCraftDID3T3_NewModel.net ..\nets\StarCraft\StarCraftDID4T3_NewModel.net
..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT4_NewModel.net ..\nets\StarCraft\StarCraftIDIDT4_5_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T4_NewModel.net ..\nets\StarCraft\StarCraftDID1T4_NewModel.net ..\nets\StarCraft\StarCraftDID2T4_NewModel.net ..\nets\StarCraft\StarCraftDID3T4_NewModel.net ..\nets\StarCraft\StarCraftDID4T4_NewModel.net

REM ..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_10_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T3_NewModel.net ..\nets\StarCraft\StarCraftDID1T3_NewModel.net ..\nets\StarCraft\StarCraftDID2T3_NewModel.net ..\nets\StarCraft\StarCraftDID3T3_NewModel.net ..\nets\StarCraft\StarCraftDID4T3_NewModel.net ..\nets\StarCraft\StarCraftDID5T3_NewModel.net ..\nets\StarCraft\StarCraftDID6T3_NewModel.net ..\nets\StarCraft\StarCraftDID7T3_NewModel.net ..\nets\StarCraft\StarCraftDID8T3_NewModel.net ..\nets\StarCraft\StarCraftDID9T3_NewModel.net
REM ..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT4_NewModel.net ..\nets\StarCraft\StarCraftIDIDT4_10_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T4_NewModel.net ..\nets\StarCraft\StarCraftDID1T4_NewModel.net ..\nets\StarCraft\StarCraftDID2T4_NewModel.net ..\nets\StarCraft\StarCraftDID3T4_NewModel.net ..\nets\StarCraft\StarCraftDID4T4_NewModel.net ..\nets\StarCraft\StarCraftDID5T4_NewModel.net ..\nets\StarCraft\StarCraftDID6T4_NewModel.net ..\nets\StarCraft\StarCraftDID7T4_NewModel.net ..\nets\StarCraft\StarCraftDID8T4_NewModel.net ..\nets\StarCraft\StarCraftDID9T4_NewModel.net

echo Generate Policies
echo DID
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID0T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM0T3\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID1T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM1T3\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID2T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM2T3\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID3T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM3T3\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID4T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM4T3\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID5T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM5T3\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID6T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM6T3\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID7T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM7T3\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID8T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM8T3\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID9T3_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM9T3\ .csv

START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID0T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM0T4\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID1T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM1T4\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID2T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM2T4\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID3T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM3T4\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID4T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM4T4\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID5T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM5T4\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID6T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM6T4\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID7T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM7T4\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID8T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM8T4\ .csv
REM ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID9T4_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM9T4\ .csv


echo I-DID
echo 2 Models
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_2_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT3_2\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT4_2_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT4_2\ .csv

echo 5 Models
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_5_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT3_5\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT4_5_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT4_5\ .csv

REM echo 10 Models
REM START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_10_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT3_10\ .csv
REM START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT4_10_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT4_10\ .csv

pause