@echo off
echo SetupDirectories
mkdir ..\PolicyFiles\DIDM0T3\
mkdir ..\PolicyFiles\DIDM1T3\
mkdir ..\PolicyFiles\DIDM2T3\
mkdir ..\PolicyFiles\DIDM3T3\
mkdir ..\PolicyFiles\DIDM4T3\

mkdir ..\PolicyFiles\DIDM0T4\
mkdir ..\PolicyFiles\DIDM1T4\
mkdir ..\PolicyFiles\DIDM2T4\
mkdir ..\PolicyFiles\DIDM3T4\
mkdir ..\PolicyFiles\DIDM4T4\

mkdir ..\PolicyFiles\DIDM0T5\
mkdir ..\PolicyFiles\DIDM1T5\
mkdir ..\PolicyFiles\DIDM2T5\
mkdir ..\PolicyFiles\DIDM3T5\
mkdir ..\PolicyFiles\DIDM4T5\

mkdir ..\PolicyFiles\DIDM0T6\
mkdir ..\PolicyFiles\DIDM1T6\
mkdir ..\PolicyFiles\DIDM2T6\
mkdir ..\PolicyFiles\DIDM3T6\
mkdir ..\PolicyFiles\DIDM4T6\

mkdir ..\PolicyFiles\IDIDT3_2\
mkdir ..\PolicyFiles\IDIDT4_2\
mkdir ..\PolicyFiles\IDIDT5_2\
mkdir ..\PolicyFiles\IDIDT6_2\
mkdir ..\PolicyFiles\IDIDT3_5\
mkdir ..\PolicyFiles\IDIDT4_5\
mkdir ..\PolicyFiles\IDIDT5_5\
mkdir ..\PolicyFiles\IDIDT6_5\

echo Expand Models
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID4.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID5.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID6.net 3 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT4-0.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT4-1.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT4-2.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT4-3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT4-4.net 1 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT5-0.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT5-1.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT5-2.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT5-3.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT5-4.net 2 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT6-0.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT6-1.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT6-2.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT6-3.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT6-4.net 3 LIMID


echo Enter DID Models Into I-DID
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID3.net ..\nets\TIDID3_2.net aj mod oj ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID4.net ..\nets\TIDID4_2.net aj mod oj ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID5.net ..\nets\TIDID5_2.net aj mod oj ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID6.net ..\nets\TIDID6_2.net aj mod oj ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net

..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID3.net ..\nets\TIDID3_5.net aj mod oj ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID4.net ..\nets\TIDID4_5.net aj mod oj ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID5.net ..\nets\TIDID5_5.net aj mod oj ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\TIDID6.net ..\nets\TIDID6_5.net aj mod oj ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net

echo Generate Policies
echo DID
echo T=3
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT3-0.net aj oj CSV ..\PolicyFiles\DIDM0T3\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT3-1.net aj oj CSV ..\PolicyFiles\DIDM1T3\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT3-2.net aj oj CSV ..\PolicyFiles\DIDM2T3\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT3-3.net aj oj CSV ..\PolicyFiles\DIDM3T3\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT3-4.net aj oj CSV ..\PolicyFiles\DIDM4T3\

echo T=4
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT4-0.net aj oj CSV ..\PolicyFiles\DIDM0T4\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT4-1.net aj oj CSV ..\PolicyFiles\DIDM1T4\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT4-2.net aj oj CSV ..\PolicyFiles\DIDM2T4\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT4-3.net aj oj CSV ..\PolicyFiles\DIDM3T4\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT4-4.net aj oj CSV ..\PolicyFiles\DIDM4T4\

echo T=5
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT5-0.net aj oj CSV ..\PolicyFiles\DIDM0T5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT5-1.net aj oj CSV ..\PolicyFiles\DIDM1T5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT5-2.net aj oj CSV ..\PolicyFiles\DIDM2T5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT5-3.net aj oj CSV ..\PolicyFiles\DIDM3T5\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT5-4.net aj oj CSV ..\PolicyFiles\DIDM4T5\

echo T=6
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT6-0.net aj oj CSV ..\PolicyFiles\DIDM0T6\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT6-1.net aj oj CSV ..\PolicyFiles\DIDM1T6\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT6-2.net aj oj CSV ..\PolicyFiles\DIDM2T6\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT6-3.net aj oj CSV ..\PolicyFiles\DIDM3T6\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\tigerDIDT6-4.net aj oj CSV ..\PolicyFiles\DIDM4T6\

echo I-DID
echo 2 Models
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID3_2.net ai oi CSV ..\PolicyFiles\IDIDT3_2\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID4_2.net ai oi CSV ..\PolicyFiles\IDIDT4_2\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID5_2.net ai oi CSV ..\PolicyFiles\IDIDT5_2\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID6_2.net ai oi CSV ..\PolicyFiles\IDIDT6_2\

echo 5 Models
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID3_5.net ai oi CSV ..\PolicyFiles\IDIDT3_5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID4_5.net ai oi CSV ..\PolicyFiles\IDIDT4_5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID5_5.net ai oi CSV ..\PolicyFiles\IDIDT5_5\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\TIDID6_5.net ai oi CSV ..\PolicyFiles\IDIDT6_5\

pause