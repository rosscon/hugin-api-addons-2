@echo off
echo SetupDirectories

mkdir ..\PolicyFiles\DIDM0T5\
mkdir ..\PolicyFiles\DIDM1T5\
mkdir ..\PolicyFiles\DIDM2T5\
mkdir ..\PolicyFiles\DIDM3T5\
mkdir ..\PolicyFiles\DIDM4T5\

mkdir ..\PolicyFiles\IDIDT5_2\
mkdir ..\PolicyFiles\IDIDT5_5\
mkdir ..\nets\StarCraft\

echo Expand Models
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID0_NewModel.net ..\nets\StarCraft\StarCraftDID0T5_NewModel.net LIMID 3
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID1_NewModel.net ..\nets\StarCraft\StarCraftDID1T5_NewModel.net LIMID 3
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID2_NewModel.net ..\nets\StarCraft\StarCraftDID2T5_NewModel.net LIMID 3
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID3_NewModel.net ..\nets\StarCraft\StarCraftDID3T5_NewModel.net LIMID 3
..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftDID4_NewModel.net ..\nets\StarCraft\StarCraftDID4T5_NewModel.net LIMID 3

..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftIDID_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net LIMID 3

echo Enter DID Models Into I-DID
..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_2_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T5_NewModel.net ..\nets\StarCraft\StarCraftDID1T5_NewModel.net
..\HuginApiAddonsTesters.exe expandIDID ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_5_NewModel.net [siUnderAttack,ui,oiSeeEnemy,ojSeeEnemy,ai,aj,modj] ..\nets\StarCraft\StarCraftDID0T5_NewModel.net ..\nets\StarCraft\StarCraftDID1T5_NewModel.net ..\nets\StarCraft\StarCraftDID2T5_NewModel.net ..\nets\StarCraft\StarCraftDID3T5_NewModel.net ..\nets\StarCraft\StarCraftDID4T5_NewModel.net

echo Generate Policies
echo DID
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID0T5_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM0T5\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID1T5_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM1T5\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID2T5_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM2T5\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID3T5_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM3T5\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftDID4T5_NewModel.net aj ojSeeEnemy ..\PolicyFiles\DIDM4T5\ .csv

echo I-DID
echo 2 Models
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_2_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_2\ .csv

echo 5 Models
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_5_NewModel.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_5\ .csv

pause