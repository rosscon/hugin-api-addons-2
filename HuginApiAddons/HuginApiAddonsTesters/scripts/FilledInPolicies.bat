@echo off
set t3models=..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net
REM ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
set t4models=..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
REM ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
set t5models=..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net
REM ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net

echo Filled In Policies
echo T = 3
echo 15
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_15.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_15.net %t3models%
echo 30
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_30.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_30.net %t3models%
echo 45
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_45.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_45.net %t3models%
echo 60
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_60.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_60.net %t3models%
echo 75
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_75.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_75.net %t3models%
echo 90
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_90.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_90.net %t3models%

echo T = 4
echo 15
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_15.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_15.net %t4models%
echo 30
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_30.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_30.net %t4models%
echo 45
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_45.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_45.net %t4models%
echo 60
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_60.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_60.net %t4models%
echo 75
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_75.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_75.net %t4models%
echo 90
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_90.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_90.net %t4models%

REM  echo T = 5
REM  echo 15
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_15.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_15.net %t5models%
REM  echo 30
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_30.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_30.net %t5models%
REM  echo 45
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_45.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_45.net %t5models%
REM  echo 60
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_60.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_60.net %t5models%
REM  echo 75
REM  timeout /t 5
REM  echo 90
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_90.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_90.net %t5models%