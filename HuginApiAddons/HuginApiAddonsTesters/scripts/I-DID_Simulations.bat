@echo off

echo I-DID Simulations
echo:
echo:
echo Exact Policies
start cmd /C ExactPolicies.bat
echo Partial Policies
timeout /t 5
start cmd /C PartialPolicies.bat
echo Filled In Policies
timeout /t 5
start cmd /C FilledInPolicies.bat



pause