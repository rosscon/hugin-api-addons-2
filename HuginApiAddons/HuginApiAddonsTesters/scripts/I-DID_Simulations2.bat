@echo off
set t3models=..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT3-5.net ..\nets\tigerDIDT3-6.net ..\nets\tigerDIDT3-7.net ..\nets\tigerDIDT3-8.net ..\nets\tigerDIDT3-9.net 
REM ..\nets\tigerDIDT3-10.net ..\nets\tigerDIDT3-11.net ..\nets\tigerDIDT3-12.net ..\nets\tigerDIDT3-13.net ..\nets\tigerDIDT3-14.net 
REM ..\nets\tigerDIDT3-15.net ..\nets\tigerDIDT3-16.net ..\nets\tigerDIDT3-17.net ..\nets\tigerDIDT3-18.net ..\nets\tigerDIDT3-19.net

set t4models=..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net ..\nets\tigerDIDT4-5.net ..\nets\tigerDIDT4-6.net ..\nets\tigerDIDT4-7.net ..\nets\tigerDIDT4-8.net ..\nets\tigerDIDT4-9.net 
REM ..\nets\tigerDIDT4-10.net ..\nets\tigerDIDT4-11.net ..\nets\tigerDIDT4-12.net ..\nets\tigerDIDT4-13.net ..\nets\tigerDIDT4-14.net 
REM ..\nets\tigerDIDT4-15.net ..\nets\tigerDIDT4-16.net ..\nets\tigerDIDT4-17.net ..\nets\tigerDIDT4-18.net ..\nets\tigerDIDT4-19.net

set t5models=..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net 
REM ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net

echo I-DID Simulations
echo:
echo:
echo Exact Policies
start cmd /C ExactPolicies2.bat
echo Partial Policies
timeout /t 5

echo Partial Policies
echo T = 3
REM  echo 5
REM  imeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_05.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID3_Partial_05.net %t3models%
REM  echo 10
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_10.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID3_Partial_10.net %t3models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_20.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID3_Partial_20.net %t3models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_100.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID3_Partial_100.net %t3models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_150.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID3_Partial_150.net %t3models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_200.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID3_Partial_200.net %t3models%

echo T = 4
REM  echo 5
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_05.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID4_Partial_05.net %t4models%
REM  echo 10
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_10.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID4_Partial_10.net %t4models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_20.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID4_Partial_20.net %t4models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_100.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID4_Partial_100.net %t4models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_150.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID4_Partial_150.net %t4models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_200.csv [s,u,oi,oj,ai,aj] 500 I-DID ..\nets\TIDID4_Partial_200.net %t4models%

REM  echo T = 5
REM  echo 20
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_20.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_20.net %t5models%
REM  echo 100
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_100.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_100.net %t5models%
REM  echo 150
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_150.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_150.net %t5models%
REM  echo 200
REM  timeout /t 5
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_200.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_200.net %t5models%

echo Filled In Policies
echo T = 3
REM  echo 5
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_05.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_05.net %t3models%
REM  echo 10
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_10.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_10.net %t3models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_20.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_20.net %t3models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_100.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_100.net %t3models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_150.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_150.net %t3models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T3_Filled_200.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Filled_200.net %t3models%

echo T = 4
REM  echo 5
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_05.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_05.net %t4models%
REM  echo 10
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_10.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_10.net %t4models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_20.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_20.net %t4models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_100.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_100.net %t4models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_150.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_150.net %t4models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T4_Filled_200.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Filled_200.net %t4models%

REM  echo T = 5
REM  echo 20
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_20.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_20.net %t5models%
REM  echo 100
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_100.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_100.net %t5models%
REM  echo 150
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_150.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_150.net %t5models%
REM  echo 200
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Filled_200.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Filled_200.net %t5models%

