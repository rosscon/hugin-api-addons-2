@echo off
echo Learn Policies
TITLE Learning Policies I-DIDs
set maxTree=3
set timeStepDuration=1000
set directory=..\replay\
set landMark=LandmarkHeath

..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftIDID_NewModel_Rep.net ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net LIMID 1

echo 1
set	extractedReplays=%directory%Terran Goliath_215.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_01.net [aj,modj]

echo 2
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_218.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_02.net [aj,modj]

echo 5
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_695.csv
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_700.csv
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_795.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_05.net [aj,modj]

echo 7
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_857.csv
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_876.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_07.net [aj,modj]


REM echo 10
REM set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_894.csv
REM set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_927.csv
REM set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_928.csv
REM ..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_10.net [aj,modj]

REM echo 15

TITLE Learning Policies Opponent Policies
..\HuginApiAddonsTesters.exe learnPolicySCReplayPolicyFiles "[%extractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] 1000 %maxTree% aj ..\PolicyFiles\Replay\

TITLE Generate I-DID Policies
echo Generate I-DID Policies

mkdir ..\PolicyFiles\Replay\IDID01\
mkdir ..\PolicyFiles\Replay\IDID02\
mkdir ..\PolicyFiles\Replay\IDID05\
mkdir ..\PolicyFiles\Replay\IDID07\
mkdir ..\PolicyFiles\Replay\IDID10\

START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_01.net ai oiSeeEnemy ..\PolicyFiles\Replay\IDID01\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_02.net ai oiSeeEnemy ..\PolicyFiles\Replay\IDID02\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_05.net ai oiSeeEnemy ..\PolicyFiles\Replay\IDID05\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_07.net ai oiSeeEnemy ..\PolicyFiles\Replay\IDID07\ .csv
..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT4_Rep_NewModel_10.net ai oiSeeEnemy ..\PolicyFiles\Replay\IDID10\ .csv


pause