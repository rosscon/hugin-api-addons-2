@echo off
echo Learn Policies
TITLE Learning Policies I-DIDs
set maxTree=3
set timeStepDuration=1000
set directory=..\replay\
set landMark=LandmarkHeath

..\HuginApiAddonsTesters.exe  expand ..\nets\StarCraftIDID_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net LIMID 1

echo 1
set	extractedReplays=%directory%Terran Goliath_215.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 2
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_218.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 3
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_695.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 4
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_700.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 5
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_795.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 6
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_857.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 7
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_876.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 8
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_894.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 9
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_927.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]
echo 10
set	extractedReplays=%ExtractedReplays%,%directory%Terran Goliath_928.csv
..\HuginApiAddonsTesters.exe learnPolicySCReplayIDID "[%ExtractedReplays%]" [Action,Observation,GameTime] [%landmark%] [DontSee,SeeFar,SeeNear] [Attack,Escape,Nothing] %timeStepDuration% %maxTree% ..\nets\StarCraft\StarCraftIDIDT3_NewModel.net ..\nets\StarCraft\StarCraftIDIDT3_Rep_NewModel_TIME.net [aj,modj]


pause