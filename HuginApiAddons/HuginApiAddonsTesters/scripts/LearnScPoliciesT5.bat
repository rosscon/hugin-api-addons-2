@echo off
set epsilon=1.5

echo Learn Models
echo 2 Models
echo T=5
echo 1 Battle
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod2_01.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_01.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_01.net %epsilon% ..\csvFiles\mergesSC.csv
echo 2 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod2_02.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_02.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_02.net %epsilon% ..\csvFiles\mergesSC.csv
echo 5 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod2_05.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_05.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_05.net %epsilon% ..\csvFiles\mergesSC.csv
echo 10 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod2_10.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_10.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_10.net %epsilon% ..\csvFiles\mergesSC.csv
echo 20 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod2_20.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_20.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_20.net %epsilon% ..\csvFiles\mergesSC.csv

echo Generate Policies
mkdir ..\PolicyFiles\IDIDT5_L_M2_01\
mkdir ..\PolicyFiles\IDIDT5_L_M2_02\
mkdir ..\PolicyFiles\IDIDT5_L_M2_05\
mkdir ..\PolicyFiles\IDIDT5_L_M2_10\
mkdir ..\PolicyFiles\IDIDT5_L_M2_20\
mkdir ..\PolicyFiles\IDIDT5_F_M2_01\
mkdir ..\PolicyFiles\IDIDT5_F_M2_02\
mkdir ..\PolicyFiles\IDIDT5_F_M2_05\
mkdir ..\PolicyFiles\IDIDT5_F_M2_10\
mkdir ..\PolicyFiles\IDIDT5_F_M2_20\

echo T=4
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_01.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M2_01\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_02.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M2_02\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_05.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M2_05\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_10.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M2_10\ .csv
..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_20.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M2_20\ .csv

START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_NewModel_Filled_01.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M2_01\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_NewModel_Filled_02.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M2_02\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_NewModel_Filled_05.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M2_05\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_NewModel_Filled_10.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M2_10\ .csv
..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT3_NewModel_Filled_20.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M2_20\ .csv


echo 5 Models
echo T=4
echo 1 Battle
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod5_01.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_01.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_01.net %epsilon% ..\csvFiles\mergesSC.csv
echo 2 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod5_02.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_02.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_02.net %epsilon% ..\csvFiles\mergesSC.csv
echo 5 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod5_05.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_05.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_05.net %epsilon% ..\csvFiles\mergesSC.csv
echo 7 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod5_07.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_07.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_07.net %epsilon% ..\csvFiles\mergesSC.csv
echo 10 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod5_10.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_10.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_10.net %epsilon% ..\csvFiles\mergesSC.csv
echo 20 Battles
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\t5_did_training_mod5_20.csv  [Model,State,Observation,Action] [SeeNear,SeeFar,DontSee] [] [] ..\nets\StarCraft\StarCraftIDIDT5_NewModel.net ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_20.net [aj,modj] 4 ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_20.net %epsilon% ..\csvFiles\mergesSC.csv

echo Generate Policies
mkdir ..\PolicyFiles\IDIDT5_L_M5_01\
mkdir ..\PolicyFiles\IDIDT5_L_M5_02\
mkdir ..\PolicyFiles\IDIDT5_L_M5_05\
mkdir ..\PolicyFiles\IDIDT5_L_M5_10\
mkdir ..\PolicyFiles\IDIDT5_L_M5_20\
mkdir ..\PolicyFiles\IDIDT5_F_M5_01\
mkdir ..\PolicyFiles\IDIDT5_F_M5_02\
mkdir ..\PolicyFiles\IDIDT5_F_M5_05\
mkdir ..\PolicyFiles\IDIDT5_F_M5_10\
mkdir ..\PolicyFiles\IDIDT5_F_M5_20\

echo T=4
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_01.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M5_01\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_02.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M5_02\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_05.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M5_05\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_10.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M5_10\ .csv
..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Learnt_20.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_L_M5_20\ .csv

START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_01.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M5_01\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_02.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M5_02\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_05.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M5_05\ .csv
START ..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_10.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M5_10\ .csv
..\HuginApiAddonsTesters.exe policyWriter ..\nets\StarCraft\StarCraftIDIDT5_NewModel_Filled_20.net ai oiSeeEnemy ..\PolicyFiles\IDIDT5_F_M5_20\ .csv

pause