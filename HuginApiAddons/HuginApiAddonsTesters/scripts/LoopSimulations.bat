@echo off
set numLoops=10


echo Mod 5
for /L %%n in (1,1,%numloops%) do (
	@Title Mod 5 Loop %%n / %numloops%
	echo Loop %%n  
	START /wait cmd.exe /c simulationsMod05.bat
)

echo Mod 10
for /L %%n in (1,1,%numloops%) do (
	@Title Mod 10 Loop %%n / %numloops%
	echo Loop %%n  
	START /wait cmd.exe /c simulationsMod10.bat
)

echo Mod 15
for /L %%n in (1,1,%numloops%) do (
	@Title Mod 15 Loop %%n / %numloops%
	echo Loop %%n  
	START /wait cmd.exe /c simulationsMod15.bat
)

pause