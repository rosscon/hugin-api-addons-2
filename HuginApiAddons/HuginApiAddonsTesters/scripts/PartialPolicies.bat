@echo off
set t3models=..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net
REM ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
set t4models=..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
REM ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
set t5models=..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net
REM ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net

echo Partial Policies
echo T = 3
echo 15
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_15.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Partial_15.net %t3models%
echo 30
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_30.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Partial_30.net %t3models%
echo 45
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_45.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Partial_45.net %t3models%
echo 60
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_60.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Partial_60.net %t3models%
echo 75
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_75.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Partial_75.net %t3models%
echo 90
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T3_Partial_90.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID3_Partial_90.net %t3models%

echo T = 4
echo 15
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_15.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Partial_15.net %t4models%
echo 30
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_30.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Partial_30.net %t4models%
echo 45
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_45.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Partial_45.net %t4models%
echo 60
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_60.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Partial_60.net %t4models%
echo 75
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_75.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Partial_75.net %t4models%
echo 90
timeout /t 5
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_simulations_T4_Partial_90.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID4_Partial_90.net %t4models%

REM  echo T = 5
REM  echo 15
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_15.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_15.net %t5models%
REM  echo 30
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_30.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_30.net %t5models%
REM  echo 45
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_45.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_45.net %t5models%
REM  echo 60
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_60.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_60.net %t5models%
REM  echo 75
REM  timeout /t 5
REM  start cmd /C ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_75.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_75.net %t5models%
REM  echo 90
REM  timeout /t 5
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\I-DID_Simulations_T5_Partial_90.csv [s,u,oi,oj,ai,aj] 100 I-DID ..\nets\TIDID5_Partial_90.net %t5models%