@echo off 

REM Meant to be run in a directory that you choose. But DO NOT run this in the Windows directory! 

REM Prefix to add to the filename 
set strPrefix=05_

REM Set this to be the files that you want to prefix with the "strPrefix" above "*.*" means all the files 
REM set fname=*.* 
set fname=*.csv

REM List the files to be renamed 
for %%f in (%fname%) DO echo Rename file "%%f" to "%strPrefix%%%f" 

REM Rename the files 
REM 
for %%f in (%fname%) DO ren "%%f" "%strPrefix%%%f"