@echo off
COLOR 17
set NumBattles=10
set TimeStepDur=1500
set Seed=89565

echo DID Agent
echo 2 Models
echo T=3
TITLE DID Battles 2 Models (T=3, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 2 Models (T=3, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=3, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 2 Models Exact (T=3)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
echo T=5
TITLE DID Battles 2 Models (T=5, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 2 Models (T=5, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=5, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 2 Models Exact (T=5)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
echo T=7
TITLE DID Battles 2 Models (T=7, Learnt from 1 Battle)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 2 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 5 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 10 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 15 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 20 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 2 Models (T=7, Learnt from 1 Battle)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 2 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 5 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 10 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 15 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 2 Models (T=7, Learnt from 20 Battles)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 2 Models Exact (T=7)
REM ..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%


echo 5 Models
echo T=3
TITLE DID Battles 5 Models (T=3, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 5 Models (T=3, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=3, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 5 Models Exact (T=3)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

echo T=5
TITLE DID Battles 5 Models (T=5, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 5 Models (T=5, Learnt from 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%
TITLE DID Battles 5 Models (T=5, Learnt from 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%

TITLE DID Battles 5 Models Exact (T=5)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\battle.csv %NumBattles% %Seed%


pause