@echo off
echo SetupDirectories
mkdir ..\PolicyFiles\SC_DID_M0_T3\
mkdir ..\PolicyFiles\SC_DID_M1_T3\
mkdir ..\PolicyFiles\SC_DID_M2_T3\
mkdir ..\PolicyFiles\SC_DID_M3_T3\
mkdir ..\PolicyFiles\SC_DID_M4_T3\

mkdir ..\PolicyFiles\SC_DID_M0_T5\
mkdir ..\PolicyFiles\SC_DID_M1_T5\
mkdir ..\PolicyFiles\SC_DID_M2_T5\
mkdir ..\PolicyFiles\SC_DID_M3_T5\
mkdir ..\PolicyFiles\SC_DID_M4_T5\

mkdir ..\PolicyFiles\SC_DID_M0_T7\
mkdir ..\PolicyFiles\SC_DID_M1_T7\
mkdir ..\PolicyFiles\SC_DID_M2_T7\
mkdir ..\PolicyFiles\SC_DID_M3_T7\
mkdir ..\PolicyFiles\SC_DID_M4_T7\

mkdir ..\PolicyFiles\SC_IDID_M2_T3_EX\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_EX\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_EX\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_EX\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_EX\

mkdir ..\nets\StarCraft\

echo Expand Models
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID0_NewModel.net ..\nets\StarCraft\StarCraftDID0_T3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID1_NewModel.net ..\nets\StarCraft\StarCraftDID1_T3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID2_NewModel.net ..\nets\StarCraft\StarCraftDID2_T3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID3_NewModel.net ..\nets\StarCraft\StarCraftDID3_T3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID4_NewModel.net ..\nets\StarCraft\StarCraftDID4_T3.net 1 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID0_NewModel.net ..\nets\StarCraft\StarCraftDID0_T5.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID1_NewModel.net ..\nets\StarCraft\StarCraftDID1_T5.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID2_NewModel.net ..\nets\StarCraft\StarCraftDID2_T5.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID3_NewModel.net ..\nets\StarCraft\StarCraftDID3_T5.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID4_NewModel.net ..\nets\StarCraft\StarCraftDID4_T5.net 3 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID0_NewModel.net ..\nets\StarCraft\StarCraftDID0_T7.net 5 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID1_NewModel.net ..\nets\StarCraft\StarCraftDID1_T7.net 5 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID2_NewModel.net ..\nets\StarCraft\StarCraftDID2_T7.net 5 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID3_NewModel.net ..\nets\StarCraft\StarCraftDID3_T7.net 5 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftDID4_NewModel.net ..\nets\StarCraft\StarCraftDID4_T7.net 5 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftIDID_NewModel.net ..\nets\StarCraft\StarCraftIDID_T3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftIDID_NewModel.net ..\nets\StarCraft\StarCraftIDID_T5.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\StarCraftIDID_NewModel.net ..\nets\StarCraft\StarCraftIDID_T7.net 5 LIMID

echo Enter DID Models Into I-DID
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_EX2.net aj modj ojSeeEnemy ..\nets\StarCraft\StarCraftDID0_T3.net ..\nets\StarCraft\StarCraftDID1_T3.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_EX2.net aj modj ojSeeEnemy ..\nets\StarCraft\StarCraftDID0_T5.net ..\nets\StarCraft\StarCraftDID1_T5.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\StarCraft\StarCraftIDID_T7.net ..\nets\StarCraft\StarCraftIDID_T7_EX2.net aj modj ojSeeEnemy ..\nets\StarCraft\StarCraftDID0_T7.net ..\nets\StarCraft\StarCraftDID1_T7.net

..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_EX5.net aj modj ojSeeEnemy ..\nets\StarCraft\StarCraftDID0_T3.net ..\nets\StarCraft\StarCraftDID1_T3.net ..\nets\StarCraft\StarCraftDID2_T3.net ..\nets\StarCraft\StarCraftDID3_T3.net ..\nets\StarCraft\StarCraftDID4_T3.net
..\HuginApiAddonsTestCS.exe IdidExpandModels "NET" ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_EX5.net aj modj ojSeeEnemy ..\nets\StarCraft\StarCraftDID0_T5.net ..\nets\StarCraft\StarCraftDID1_T5.net ..\nets\StarCraft\StarCraftDID2_T5.net ..\nets\StarCraft\StarCraftDID3_T5.net ..\nets\StarCraft\StarCraftDID4_T5.net

echo Generate Policies
echo DID
echo T=3
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID0_T3.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M0_T3\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID1_T3.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M1_T3\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID2_T3.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M2_T3\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID3_T3.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M3_T3\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID4_T3.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M4_T3\

echo T=5
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID0_T5.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M0_T5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID1_T5.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M1_T5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID2_T5.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M2_T5\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID3_T5.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M3_T5\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID4_T5.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M4_T5\

echo T=7
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID0_T7.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M0_T7\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID1_T7.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M1_T7\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID2_T7.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M2_T7\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID3_T7.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M3_T7\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftDID4_T7.net aj ojSeeEnemy CSV ..\PolicyFiles\SC_DID_M4_T7\


echo I-DID
echo 2 Models
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_EX2.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_EX\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_EX2.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_EX\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_EX2.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_EX\

echo 5 Models
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_EX5.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_EX\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_EX5.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_EX\

pause