@echo off
COLOR 0A
set NumBattles=10
set TimeStepDur=1500

echo I-DID Agent
echo 2 Models
echo T=3
TITLE I-DID Battles 2 Models (T=3, Learnt from 1 Battle Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L01\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L01.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 2 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L02\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L02.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 5 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L05\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L05.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 10 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L10\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L10.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 15 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L15\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L15.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 20 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L20\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L20.csv %NumBattles%

TITLE I-DID Battles 2 Models (T=3, Learnt from 1 Battle Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L01_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L01_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 2 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L02_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L02_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 5 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L05_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L05_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 10 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L10_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L10_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 15 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L15_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L15_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=3, Learnt from 20 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_L20_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_L20_Fil.csv %NumBattles%

TITLE I-DID Battles 2 Models (T=3, Exact)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T3_EX\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M2_EX.csv %NumBattles%
echo T=5
TITLE I-DID Battles 2 Models (T=5, Learnt from 1 Battle Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L01\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L01.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 2 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L02\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L02.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 5 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L05\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L05.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 10 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L10\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L10.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 15 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L15\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L15.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 20 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L20\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L20.csv %NumBattles%

TITLE I-DID Battles 2 Models (T=5, Learnt from 1 Battle Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L01_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L01_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 2 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L02_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L02_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 5 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L05_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L05_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 10 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L10_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L10_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 15 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L15_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L15_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=5, Learnt from 20 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_L20_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_L20_Fil.csv %NumBattles%

TITLE I-DID Battles 2 Models (T=5, Exact)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T5_EX\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M2_EX.csv %NumBattles%
echo T=7
TITLE I-DID Battles 2 Models (T=7, Learnt from 1 Battle Random Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L01\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L01.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 2 Battles Random Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L02\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L02.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 5 Battles Random Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L05\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L05.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 10 Battles Random Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L10\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L10.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 15 Battles Random Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L15\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L15.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 20 Battles Random Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L20\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L20.csv %NumBattles%

TITLE I-DID Battles 2 Models (T=7, Learnt from 1 Battle Compatible Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L01_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L01_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 2 Battles Compatible Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L02_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L02_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 5 Battles Compatible Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L05_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L05_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 10 Battles Compatible Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L10_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L10_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 15 Battles Compatible Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L15_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L15_Fil.csv %NumBattles%
TITLE I-DID Battles 2 Models (T=7, Learnt from 20 Battles Compatible Fill)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_L20_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_L20_Fil.csv %NumBattles%

TITLE I-DID Battles 2 Models (T=7, Exact)
REM ..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M2_T7_EX\ .csv %TimeStepDur% ai ..\csvFiles\SC_T7_M2_EX.csv %NumBattles%


echo 5 Models
echo T=3
TITLE I-DID Battles 5 Models (T=3, Learnt from 1 Battle Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L01\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L01.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 2 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L02\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L02.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 5 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L05\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L05.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 10 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L10\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L10.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 15 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L15\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L15.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 20 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L20\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L20.csv %NumBattles%

TITLE I-DID Battles 5 Models (T=3, Learnt from 1 Battle Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L01_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L01_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 2 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L02_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L02_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 5 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L05_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L05_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 10 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L10_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L10_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 15 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L15_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L15_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=3, Learnt from 20 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_L20_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_L20_Fil.csv %NumBattles%

TITLE I-DID Battles 5 Models (T=3, Exact)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T3_EX\ .csv %TimeStepDur% ai ..\csvFiles\SC_T3_M5_EX.csv %NumBattles%
echo T=5
TITLE I-DID Battles 5 Models (T=5, Learnt from 1 Battle Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L01\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L01.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 2 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L02\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L02.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 5 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L05\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L05.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 10 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L10\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L10.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 15 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L15\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L15.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 20 Battles Random Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L20\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L20.csv %NumBattles%

TITLE I-DID Battles 5 Models (T=5, Learnt from 1 Battle Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L01_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L01_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 2 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L02_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L02_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 5 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L05_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L05_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 10 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L10_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L10_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 15 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L15_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L15_Fil.csv %NumBattles%
TITLE I-DID Battles 5 Models (T=5, Learnt from 20 Battles Compatible Fill)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_L20_Fil\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_L20_Fil.csv %NumBattles%

TITLE I-DID Battles 5 Models (T=5, Exact)
..\StarcraftIdid.exe single ..\PolicyFiles\SC_IDID_M5_T5_EX\ .csv %TimeStepDur% ai ..\csvFiles\SC_T5_M5_EX.csv %NumBattles%

pause