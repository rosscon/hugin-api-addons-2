@echo off

set epsilon=0.5

echo SetupDirectories
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L01\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L02\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L05\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L10\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L15\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L20\

mkdir ..\PolicyFiles\SC_IDID_M2_T3_L01_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L02_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L05_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L10_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L15_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T3_L20_Fil\

mkdir ..\PolicyFiles\SC_IDID_M2_T5_L01\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L02\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L05\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L10\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L15\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L20\

mkdir ..\PolicyFiles\SC_IDID_M2_T5_L01_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L02_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L05_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L10_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L15_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T5_L20_Fil\

mkdir ..\PolicyFiles\SC_IDID_M2_T7_L01\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L02\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L05\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L10\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L15\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L20\

mkdir ..\PolicyFiles\SC_IDID_M2_T7_L01_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L02_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L05_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L10_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L15_Fil\
mkdir ..\PolicyFiles\SC_IDID_M2_T7_L20_Fil\

mkdir ..\PolicyFiles\SC_IDID_M5_T3_L01\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L02\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L05\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L10\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L15\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L20\

mkdir ..\PolicyFiles\SC_IDID_M5_T3_L01_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L02_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L05_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L10_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L15_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T3_L20_Fil\

mkdir ..\PolicyFiles\SC_IDID_M5_T5_L01\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L02\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L05\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L10\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L15\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L20\

mkdir ..\PolicyFiles\SC_IDID_M5_T5_L01_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L02_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L05_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L10_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L15_Fil\
mkdir ..\PolicyFiles\SC_IDID_M5_T5_L20_Fil\


echo Learn Trees
echo 2 Models
echo T=3
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod2_01.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M2_L01.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M2_L01_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod2_02.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M2_L02.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M2_L02_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod2_05.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M2_L05.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M2_L05_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod2_10.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M2_L10.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M2_L10_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod2_15.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M2_L15.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M2_L15_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod2_20.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M2_L20.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M2_L20_Fil.net SIM %epsilon%
echo T=5
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod2_01.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M2_L01.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M2_L01_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod2_02.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M2_L02.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M2_L02_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod2_05.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M2_L05.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M2_L05_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod2_10.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M2_L10.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M2_L10_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod2_15.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M2_L15.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M2_L15_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod2_20.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M2_L20.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M2_L20_Fil.net SIM %epsilon%
REM echo T=7
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t7_did_training_mod2_01.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T7.net ..\nets\StarCraft\StarCraftIDID_T7_M2_L01.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T7_M2_L01_Fil.net SIM %epsilon%
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t7_did_training_mod2_02.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T7.net ..\nets\StarCraft\StarCraftIDID_T7_M2_L02.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T7_M2_L02_Fil.net SIM %epsilon%
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t7_did_training_mod2_05.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T7.net ..\nets\StarCraft\StarCraftIDID_T7_M2_L05.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T7_M2_L05_Fil.net SIM %epsilon%
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t7_did_training_mod2_10.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T7.net ..\nets\StarCraft\StarCraftIDID_T7_M2_L10.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T7_M2_L10_Fil.net SIM %epsilon%
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t7_did_training_mod2_15.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T7.net ..\nets\StarCraft\StarCraftIDID_T7_M2_L15.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T7_M2_L15_Fil.net SIM %epsilon%
REM ..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t7_did_training_mod2_20.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T7.net ..\nets\StarCraft\StarCraftIDID_T7_M2_L20.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T7_M2_L20_Fil.net SIM %epsilon%

echo 5 Models
echo T=3
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod5_01.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M5_L01.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M5_L01_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod5_02.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M5_L02.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M5_L02_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod5_05.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M5_L05.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M5_L05_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod5_10.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M5_L10.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M5_L10_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod5_15.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M5_L15.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M5_L15_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t3_did_training_mod5_20.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T3.net ..\nets\StarCraft\StarCraftIDID_T3_M5_L20.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T3_M5_L20_Fil.net SIM %epsilon%
echo T=5
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod5_01.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M5_L01.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M5_L01_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod5_02.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M5_L02.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M5_L02_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod5_05.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M5_L05.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M5_L05_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod5_10.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M5_L10.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M5_L10_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod5_15.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M5_L15.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M5_L15_Fil.net SIM %epsilon%
..\HuginApiAddonsTestCS.exe LearnTrees ..\csvFiles\t5_did_training_mod5_20.csv Observation SeeNear,SeeFar,DontSee Action Model TimeStep ..\nets\StarCraft\StarCraftIDID_T5.net ..\nets\StarCraft\StarCraftIDID_T5_M5_L20.net aj ojSeeEnemy modj ..\nets\StarCraft\StarCraftIDID_T5_M5_L20_Fil.net SIM %epsilon%


echo Generate Policies
echo 2 models
echo T=3
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L01.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L01\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L02.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L02\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L05.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L05\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L10.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L10\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L15.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L15\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L20.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L20\

START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L01_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L01_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L02_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L02_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L05_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L05_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L10_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L10_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L15_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L15_Fil\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M2_L20_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T3_L20_Fil\

echo T=5
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L01.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L01\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L02.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L02\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L05.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L05\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L10.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L10\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L15.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L15\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L20.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L20\

START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L01_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L01_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L02_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L02_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L05_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L05_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L10_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L10_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L15_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L15_Fil\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M2_L20_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T5_L20_Fil\
echo T=7
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L01.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L01\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L02.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L02\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L05.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L05\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L10.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L10\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L15.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L15\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L20.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L20\

REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L01_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L01_Fil\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L02_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L02_Fil\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L05_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L05_Fil\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L10_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L10_Fil\
REM START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L15_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L15_Fil\
REM ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T7_M2_L20_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M2_T7_L20_Fil\

echo 5 models
echo T=3
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L01.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L01\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L02.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L02\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L05.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L05\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L10.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L10\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L15.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L15\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L20.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L20\

START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L01_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L01_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L02_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L02_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L05_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L05_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L10_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L10_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L15_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L15_Fil\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T3_M5_L20_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T3_L20_Fil\

echo T=5
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L01.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L01\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L02.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L02\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L05.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L05\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L10.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L10\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L15.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L15\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L20.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L20\

START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L01_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L01_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L02_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L02_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L05_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L05_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L10_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L10_Fil\
START ..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L15_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L15_Fil\
..\HuginApiAddonsTestCS.exe PolicyGenerator ..\nets\StarCraft\StarCraftIDID_T5_M5_L20_Fil.net ai oiSeeEnemy CSV ..\PolicyFiles\SC_IDID_M5_T5_L20_Fil\






