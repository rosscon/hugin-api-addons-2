@echo off
COLOR 17
set TimeStepDur=1500
set Seed=5243860

echo DID vs DID Training Battles
echo 2 Models
echo T=3
echo 1 Battle
TITLE DID vs DID Training Battles 2 Models (T=3, 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod2_01.csv 1 %Seed%
echo 2 Battles
TITLE DID vs DID Training Battles 2 Models (T=3, 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod2_02.csv 2 %Seed%
echo 5 Battles
TITLE DID vs DID Training Battles 2 Models (T=3, 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod2_05.csv 5 %Seed%
echo 10 Battles
TITLE DID vs DID Training Battles 2 Models (T=3, 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod2_10.csv 10 %Seed%
echo 15 Battles
TITLE DID vs DID Training Battles 2 Models (T=3, 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod2_15.csv 10 %Seed%
echo 20 Battles
TITLE DID vs DID Training Battles 2 Models (T=3, 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod2_20.csv 20 %Seed%

echo T=5
echo 1 Battle
TITLE DID vs DID Training Battles 2 Models (T=5, 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod2_01.csv 1 %Seed%
echo 2 Battles
TITLE DID vs DID Training Battles 2 Models (T=5, 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod2_02.csv 2 %Seed%
echo 5 Battles
TITLE DID vs DID Training Battles 2 Models (T=5, 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod2_05.csv 5 %Seed%
echo 10 Battles
TITLE DID vs DID Training Battles 2 Models (T=5, 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod2_10.csv 10 %Seed%
echo 15 Battles
TITLE DID vs DID Training Battles 2 Models (T=5, 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod2_15.csv 10 %Seed%
echo 20 Battles
TITLE DID vs DID Training Battles 2 Models (T=5, 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod2_20.csv 20 %Seed%

echo T=7
echo 1 Battle
TITLE DID vs DID Training Battles 2 Models (T=7, 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\t7_did_training_mod2_01.csv 1 %Seed%
echo 2 Battles
TITLE DID vs DID Training Battles 2 Models (T=7, 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\t7_did_training_mod2_02.csv 2 %Seed%
echo 5 Battles
TITLE DID vs DID Training Battles 2 Models (T=7, 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\t7_did_training_mod2_05.csv 5 %Seed%
echo 10 Battles
TITLE DID vs DID Training Battles 2 Models (T=7, 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\t7_did_training_mod2_10.csv 10 %Seed%
echo 15 Battles
TITLE DID vs DID Training Battles 2 Models (T=7, 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\t7_did_training_mod2_15.csv 10 %Seed%
echo 20 Battles
TITLE DID vs DID Training Battles 2 Models (T=7, 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T7\,..\PolicyFiles\SC_DID_M1_T7\," .csv %TimeStepDur% aj ..\csvFiles\t7_did_training_mod2_20.csv 20 %Seed%


echo 5 Models
echo T=3
echo 1 Battle
TITLE DID vs DID Training Battles 5 Models (T=3, 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod5_01.csv 1 %Seed%
echo 2 Battles
TITLE DID vs DID Training Battles 5 Models (T=3, 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod5_02.csv 2 %Seed%
echo 5 Battles
TITLE DID vs DID Training Battles 5 Models (T=3, 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod5_05.csv 5 %Seed%
echo 10 Battles
TITLE DID vs DID Training Battles 5 Models (T=3, 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod5_10.csv 10 %Seed%
echo 15 Battles
TITLE DID vs DID Training Battles 5 Models (T=3, 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod5_15.csv 10 %Seed%
echo 20 Battles
TITLE DID vs DID Training Battles 5 Models (T=3, 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T3\,..\PolicyFiles\SC_DID_M1_T3\,..\PolicyFiles\SC_DID_M2_T3\,..\PolicyFiles\SC_DID_M3_T3\,..\PolicyFiles\SC_DID_M4_T3\," .csv %TimeStepDur% aj ..\csvFiles\t3_did_training_mod5_20.csv 20 %Seed%

echo T=5
echo 1 Battle
TITLE DID vs DID Training Battles 5 Models (T=5, 1 Battle)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod5_01.csv 1 %Seed%
echo 2 Battles
TITLE DID vs DID Training Battles 5 Models (T=5, 2 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod5_02.csv 2 %Seed%
echo 5 Battles
TITLE DID vs DID Training Battles 5 Models (T=5, 5 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod5_05.csv 5 %Seed%
echo 10 Battles
TITLE DID vs DID Training Battles 5 Models (T=5, 10 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod5_10.csv 10 %Seed%
echo 15 Battles
TITLE DID vs DID Training Battles 5 Models (T=5, 15 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod5_15.csv 10 %Seed%
echo 20 Battles
TITLE DID vs DID Training Battles 5 Models (T=5, 20 Battles)
..\StarcraftIdid.exe multi "..\PolicyFiles\SC_DID_M0_T5\,..\PolicyFiles\SC_DID_M1_T5\,..\PolicyFiles\SC_DID_M2_T5\,..\PolicyFiles\SC_DID_M3_T5\,..\PolicyFiles\SC_DID_M4_T5\," .csv %TimeStepDur% aj ..\csvFiles\t5_did_training_mod5_20.csv 20 %Seed%



pause