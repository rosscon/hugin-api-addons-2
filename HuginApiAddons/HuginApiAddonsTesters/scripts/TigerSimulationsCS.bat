@echo off
COLOR 0A

TITLE EXPAND DOMAINS
echo Expand Domains
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID4.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID5.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\TIDID3.net ..\nets\TIDID6.net 3 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT4-0.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT4-1.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT4-2.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT4-3.net 1 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT4-4.net 1 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT5-0.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT5-1.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT5-2.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT5-3.net 2 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT5-4.net 2 LIMID

..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT6-0.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT6-1.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT6-2.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT6-3.net 3 LIMID
..\HuginApiAddonsTestCS.exe DomainExpander ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT6-4.net 3 LIMID

echo Simulations
TITLE SIMULATIONS T=3
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_10.csv 10 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_20.csv 20 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_30.csv 30 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_40.csv 40 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_50.csv 50 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_60.csv 60 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_70.csv 70 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_80.csv 80 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_90.csv 90 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_100.csv 100 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_200.csv 200 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_500.csv 500 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_1000.csv 1000 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT3M5_10000.csv 10000 ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net

TITLE SIMULATIONS T=4
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_10.csv 10 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_20.csv 20 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_30.csv 30 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_40.csv 40 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_50.csv 50 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_60.csv 60 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_70.csv 70 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_80.csv 80 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_90.csv 90 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_100.csv 100 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_200.csv 200 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_500.csv 500 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_1000.csv 1000 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT4M5_10000.csv 10000 ..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net

TITLE SIMULATIONS T=5
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_10.csv 10 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_20.csv 20 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_30.csv 30 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_40.csv 40 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_50.csv 50 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_60.csv 60 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_70.csv 70 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_80.csv 80 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_90.csv 90 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_100.csv 100 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_200.csv 200 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_500.csv 500 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_1000.csv 1000 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT5M5_10000.csv 10000 ..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net

TITLE SIMULATIONS T=6
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_10.csv 10 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_20.csv 20 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_30.csv 30 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_40.csv 40 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_50.csv 50 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_60.csv 60 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_70.csv 70 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_80.csv 80 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_90.csv 90 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_100.csv 100 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_200.csv 200 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_500.csv 500 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_1000.csv 1000 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net
..\HuginApiAddonsTestCS.exe Simulator DID s oj aj u ..\simulations\tigerT6M5_10000.csv 10000 ..\nets\tigerDIDT6-0.net ..\nets\tigerDIDT6-1.net ..\nets\tigerDIDT6-2.net ..\nets\tigerDIDT6-3.net ..\nets\tigerDIDT6-4.net

pause