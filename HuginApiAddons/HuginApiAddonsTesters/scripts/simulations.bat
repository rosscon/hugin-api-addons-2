@echo off

mkdir ..\csvFiles

set t3models=..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net
REM ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net
set t4models=..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net
REM ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net
set t5models=..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net
REM ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net
set epsilon=1.5

echo Expand DID Models
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT4-0.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT4-1.net LIMID 1
REM ...\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT4-2.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT4-3.net LIMID 1
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT4-4.net LIMID 1

..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT5-0.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT5-1.net LIMID 2
REM ...\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT5-2.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT5-3.net LIMID 2
REM ..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT5-4.net LIMID 2


echo: 
echo: 
echo DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-15.csv [s,o,a,u] 15 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-30.csv [s,o,a,u] 30 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-45.csv [s,o,a,u] 45 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-60.csv [s,o,a,u] 60 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-75.csv [s,o,a,u] 75 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-90.csv [s,o,a,u] 90 DID %t3models%
echo T = 4
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-15.csv [s,o,a,u] 15 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-30.csv [s,o,a,u] 30 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-45.csv [s,o,a,u] 45 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-60.csv [s,o,a,u] 60 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-75.csv [s,o,a,u] 75 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-90.csv [s,o,a,u] 90 DID %t4models%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-15.csv [s,o,a,u] 15 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-30.csv [s,o,a,u] 30 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-45.csv [s,o,a,u] 45 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-60.csv [s,o,a,u] 60 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-75.csv [s,o,a,u] 75 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-90.csv [s,o,a,u] 90 DID %t5models%
echo: 
echo: 
echo Learn Policies
echo Exact Policies
echo T = 3
..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID3.net ..\nets\TIDID3_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t3models%
echo T = 4
..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID4.net ..\nets\TIDID4_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t4models%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID5.net ..\nets\TIDID5_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t5models%

echo: 
echo:

echo Learn and fill trees from 15 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_15.net [aj,mod] 3 ..\nets\TIDID3_Filled_15.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_15.net [aj,mod] 4 ..\nets\TIDID4_Filled_15.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-15.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_15.net [aj,mod] 5 ..\nets\TIDID5_Filled_15.net %epsilon%

echo Learn and fill trees from 30 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_30.net [aj,mod] 3 ..\nets\TIDID3_Filled_30.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_30.net [aj,mod] 4 ..\nets\TIDID4_Filled_30.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-30.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_30.net [aj,mod] 5 ..\nets\TIDID5_Filled_30.net %epsilon%

echo Learn and fill trees from 45 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_45.net [aj,mod] 3 ..\nets\TIDID3_Filled_45.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_45.net [aj,mod] 4 ..\nets\TIDID4_Filled_45.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-45.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_45.net [aj,mod] 5 ..\nets\TIDID5_Filled_45.net %epsilon%

echo Learn and fill trees from 60 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_60.net [aj,mod] 3 ..\nets\TIDID3_Filled_60.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_60.net [aj,mod] 4 ..\nets\TIDID4_Filled_60.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-60.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_60.net [aj,mod] 5 ..\nets\TIDID5_Filled_60.net %epsilon%

echo Learn and fill trees from 75 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_75.net [aj,mod] 3 ..\nets\TIDID3_Filled_75.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_75.net [aj,mod] 4 ..\nets\TIDID4_Filled_75.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-75.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_75.net [aj,mod] 5 ..\nets\TIDID5_Filled_75.net %epsilon%

echo Learn and fill trees from 90 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_90.net [aj,mod] 3 ..\nets\TIDID3_Filled_90.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_90.net [aj,mod] 4 ..\nets\TIDID4_Filled_90.net %epsilon%
echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-90.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_90.net [aj,mod] 5 ..\nets\TIDID5_Filled_90.net %epsilon%


echo: 
echo: 

REM I-DID_Simulations.bat

pause