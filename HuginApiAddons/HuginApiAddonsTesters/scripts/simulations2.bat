@echo off

mkdir ..\csvFiles

set t3models=..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT3-5.net ..\nets\tigerDIDT3-6.net ..\nets\tigerDIDT3-7.net ..\nets\tigerDIDT3-8.net ..\nets\tigerDIDT3-9.net 
REM ..\nets\tigerDIDT3-10.net ..\nets\tigerDIDT3-11.net ..\nets\tigerDIDT3-12.net ..\nets\tigerDIDT3-13.net ..\nets\tigerDIDT3-14.net 
REM ..\nets\tigerDIDT3-15.net ..\nets\tigerDIDT3-16.net ..\nets\tigerDIDT3-17.net ..\nets\tigerDIDT3-18.net ..\nets\tigerDIDT3-19.net

set t4models=..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net ..\nets\tigerDIDT4-5.net ..\nets\tigerDIDT4-6.net ..\nets\tigerDIDT4-7.net ..\nets\tigerDIDT4-8.net ..\nets\tigerDIDT4-9.net 
REM ..\nets\tigerDIDT4-10.net ..\nets\tigerDIDT4-11.net ..\nets\tigerDIDT4-12.net ..\nets\tigerDIDT4-13.net ..\nets\tigerDIDT4-14.net 
REM ..\nets\tigerDIDT4-15.net ..\nets\tigerDIDT4-16.net ..\nets\tigerDIDT4-17.net ..\nets\tigerDIDT4-18.net ..\nets\tigerDIDT4-19.net

set t5models=..\nets\tigerDIDT5-0.net ..\nets\tigerDIDT5-1.net 
REM ..\nets\tigerDIDT5-2.net ..\nets\tigerDIDT5-3.net ..\nets\tigerDIDT5-4.net


set epsilon=1.5



echo Expand DID Models
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT4-0.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT4-1.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT4-2.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT4-3.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT4-4.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-5.net ..\nets\tigerDIDT4-5.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-6.net ..\nets\tigerDIDT4-6.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-7.net ..\nets\tigerDIDT4-7.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-8.net ..\nets\tigerDIDT4-8.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-9.net ..\nets\tigerDIDT4-9.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-10.net ..\nets\tigerDIDT4-10.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-11.net ..\nets\tigerDIDT4-11.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-12.net ..\nets\tigerDIDT4-12.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-13.net ..\nets\tigerDIDT4-13.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-14.net ..\nets\tigerDIDT4-14.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-15.net ..\nets\tigerDIDT4-15.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-16.net ..\nets\tigerDIDT4-16.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-17.net ..\nets\tigerDIDT4-17.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-18.net ..\nets\tigerDIDT4-18.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-19.net ..\nets\tigerDIDT4-19.net LIMID 1

..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT5-0.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT5-1.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT5-2.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT5-3.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT5-4.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-5.net ..\nets\tigerDIDT5-5.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-6.net ..\nets\tigerDIDT5-6.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-7.net ..\nets\tigerDIDT5-7.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-8.net ..\nets\tigerDIDT5-8.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-9.net ..\nets\tigerDIDT5-9.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-10.net ..\nets\tigerDIDT5-10.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-11.net ..\nets\tigerDIDT5-11.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-12.net ..\nets\tigerDIDT5-12.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-13.net ..\nets\tigerDIDT5-13.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-14.net ..\nets\tigerDIDT5-14.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-15.net ..\nets\tigerDIDT5-15.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-16.net ..\nets\tigerDIDT5-16.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-17.net ..\nets\tigerDIDT5-17.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-18.net ..\nets\tigerDIDT5-18.net LIMID 2
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-19.net ..\nets\tigerDIDT5-19.net LIMID 2



echo: 
echo: 
echo DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-05.csv [s,o,a,u] 5 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-10.csv [s,o,a,u] 10 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-20.csv [s,o,a,u] 20 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-100.csv [s,o,a,u] 100 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-150.csv [s,o,a,u] 150 DID %t3models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT3-200.csv [s,o,a,u] 200 DID %t3models%
echo T = 4
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-05.csv [s,o,a,u] 5 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-10.csv [s,o,a,u] 10 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-20.csv [s,o,a,u] 20 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-100.csv [s,o,a,u] 100 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-150.csv [s,o,a,u] 150 DID %t4models%
..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT4-200.csv [s,o,a,u] 200 DID %t4models%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-20.csv [s,o,a,u] 20 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-100.csv [s,o,a,u] 100 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-150.csv [s,o,a,u] 150 DID %t5models%
REM  ..\HuginApiAddonsTesters.exe simulator ..\csvFiles\DIDsimulationsT5-200.csv [s,o,a,u] 200 DID %t5models%
echo: 
echo: 
echo Learn Policies
echo Exact Policies
echo T = 3
..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID3.net ..\nets\TIDID3_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t3models%
echo T = 4
..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID4.net ..\nets\TIDID4_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t4models%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID5.net ..\nets\TIDID5_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t5models%

echo: 
echo:

echo Learn and fill trees from 5 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-05.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_05.net [aj,mod] 3 ..\nets\TIDID3_Filled_05.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-05.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_05.net [aj,mod] 4 ..\nets\TIDID4_Filled_05.net %epsilon%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-05.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_05.net [aj,mod] 5 ..\nets\TIDID5_Filled_05.net %epsilon%

echo Learn and fill trees from 10 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-10.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_10.net [aj,mod] 3 ..\nets\TIDID3_Filled_10.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-10.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_10.net [aj,mod] 4 ..\nets\TIDID4_Filled_10.net %epsilon%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-10.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_10.net [aj,mod] 5 ..\nets\TIDID5_Filled_10.net %epsilon%

echo Learn and fill trees from 20 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-20.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_20.net [aj,mod] 3 ..\nets\TIDID3_Filled_20.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-20.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_20.net [aj,mod] 4 ..\nets\TIDID4_Filled_20.net %epsilon%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-20.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_20.net [aj,mod] 5 ..\nets\TIDID5_Filled_20.net %epsilon%

echo Learn and fill trees from 100 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-100.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_100.net [aj,mod] 3 ..\nets\TIDID3_Filled_100.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-100.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_100.net [aj,mod] 4 ..\nets\TIDID4_Filled_100.net %epsilon%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-100.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_100.net [aj,mod] 5 ..\nets\TIDID5_Filled_100.net %epsilon%

echo Learn and fill trees from 150 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-150.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_150.net [aj,mod] 3 ..\nets\TIDID3_Filled_150.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-150.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_150.net [aj,mod] 4 ..\nets\TIDID4_Filled_150.net %epsilon%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-150.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_150.net [aj,mod] 5 ..\nets\TIDID5_Filled_150.net %epsilon%

echo Learn and fill trees from 200 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT3-200.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_200.net [aj,mod] 3 ..\nets\TIDID3_Filled_200.net %epsilon%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT4-200.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_200.net [aj,mod] 4 ..\nets\TIDID4_Filled_200.net %epsilon%
REM  echo T = 5
REM  ..\HuginApiAddonsTesters.exe learnPolicy ..\csvFiles\DIDsimulationsT5-200.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID5.net ..\nets\TIDID5_Partial_200.net [aj,mod] 5 ..\nets\TIDID5_Filled_200.net %epsilon%


echo: 
echo: 

I-DID_Simulations2.bat