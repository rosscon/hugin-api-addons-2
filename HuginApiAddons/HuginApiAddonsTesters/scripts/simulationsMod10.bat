@echo off

mkdir ..\csvFiles\mod10\

set t3models=..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT3-5.net ..\nets\tigerDIDT3-6.net ..\nets\tigerDIDT3-7.net ..\nets\tigerDIDT3-8.net ..\nets\tigerDIDT3-9.net
set t4models=..\nets\tigerDIDT4-0.net ..\nets\tigerDIDT4-1.net ..\nets\tigerDIDT4-2.net ..\nets\tigerDIDT4-3.net ..\nets\tigerDIDT4-4.net ..\nets\tigerDIDT4-5.net ..\nets\tigerDIDT4-6.net ..\nets\tigerDIDT4-7.net ..\nets\tigerDIDT4-8.net ..\nets\tigerDIDT4-9.net

set numSims=100
set csvDir=..\csvFiles\mod10\
set mergesFile=%csvDir%merges.csv

set epsilon=1.5



echo Expand DID Models
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-0.net ..\nets\tigerDIDT4-0.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-1.net ..\nets\tigerDIDT4-1.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-2.net ..\nets\tigerDIDT4-2.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-3.net ..\nets\tigerDIDT4-3.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-4.net ..\nets\tigerDIDT4-4.net LIMID 1

..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-5.net ..\nets\tigerDIDT4-5.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-6.net ..\nets\tigerDIDT4-6.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-7.net ..\nets\tigerDIDT4-7.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-8.net ..\nets\tigerDIDT4-8.net LIMID 1
..\HuginApiAddonsTesters.exe  expand ..\nets\tigerDIDT3-9.net ..\nets\tigerDIDT4-9.net LIMID 1



echo: 
echo: 
echo DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT3-05.csv [s,o,a,u] 5 DID %t3models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT3-10.csv [s,o,a,u] 10 DID %t3models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT3-20.csv [s,o,a,u] 20 DID %t3models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT3-100.csv [s,o,a,u] 100 DID %t3models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT3-150.csv [s,o,a,u] 150 DID %t3models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT3-200.csv [s,o,a,u] 200 DID %t3models%
echo T = 4
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT4-05.csv [s,o,a,u] 5 DID %t4models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT4-10.csv [s,o,a,u] 10 DID %t4models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT4-20.csv [s,o,a,u] 20 DID %t4models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT4-100.csv [s,o,a,u] 100 DID %t4models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT4-150.csv [s,o,a,u] 150 DID %t4models%
..\HuginApiAddonsTesters.exe simulator %csvDir%DIDsimulationsT4-200.csv [s,o,a,u] 200 DID %t4models%
echo: 
echo: 
echo Learn Policies
echo Exact Policies
echo T = 3
..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID3.net ..\nets\TIDID3_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t3models%
echo T = 4
..\HuginApiAddonsTesters.exe expandIDID ..\nets\TIDID4.net ..\nets\TIDID4_DID_Models.net [s,u,oi,oj,ai,aj,mod] %t4models%

echo: 
echo:

echo Learn and fill trees from 5 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT3-05.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_05.net [aj,mod] 3 ..\nets\TIDID3_Filled_05.net %epsilon% %mergesFile%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT4-05.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_05.net [aj,mod] 4 ..\nets\TIDID4_Filled_05.net %epsilon% %mergesFile%

echo Learn and fill trees from 10 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT3-10.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_10.net [aj,mod] 3 ..\nets\TIDID3_Filled_10.net %epsilon% %mergesFile%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT4-10.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_10.net [aj,mod] 4 ..\nets\TIDID4_Filled_10.net %epsilon% %mergesFile%

echo Learn and fill trees from 20 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT3-20.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_20.net [aj,mod] 3 ..\nets\TIDID3_Filled_20.net %epsilon% %mergesFile%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT4-20.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_20.net [aj,mod] 4 ..\nets\TIDID4_Filled_20.net %epsilon% %mergesFile%

echo Learn and fill trees from 100 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT3-100.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_100.net [aj,mod] 3 ..\nets\TIDID3_Filled_100.net %epsilon% %mergesFile%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT4-100.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_100.net [aj,mod] 4 ..\nets\TIDID4_Filled_100.net %epsilon% %mergesFile%

echo Learn and fill trees from 150 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT3-150.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_150.net [aj,mod] 3 ..\nets\TIDID3_Filled_150.net %epsilon% %mergesFile%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT4-150.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_150.net [aj,mod] 4 ..\nets\TIDID4_Filled_150.net %epsilon% %mergesFile%

echo Learn and fill trees from 200 DID Simulations
echo T = 3
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT3-200.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID3.net ..\nets\TIDID3_Partial_200.net [aj,mod] 3 ..\nets\TIDID3_Filled_200.net %epsilon% %mergesFile%
echo T = 4
..\HuginApiAddonsTesters.exe learnPolicy %csvDir%DIDsimulationsT4-200.csv  [Model,State,Observation,Action] [GL,GR] [] [] ..\nets\TIDID4.net ..\nets\TIDID4_Partial_200.net [aj,mod] 4 ..\nets\TIDID4_Filled_200.net %epsilon% %mergesFile%


echo: 
echo: 

echo I-DID Simulations
echo:
echo:
echo Exact Policies
echo T = 3
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T3_Exact.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_DID_Models.net %t3models%
echo T = 4
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T4_Exact.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_DID_Models.net %t4models%
echo Partial Policies
timeout /t 5

echo Partial Policies
echo T = 3
echo 5
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T3_Partial_05.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Partial_05.net %t3models%
echo 10
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T3_Partial_10.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Partial_10.net %t3models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T3_Partial_20.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Partial_20.net %t3models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T3_Partial_100.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Partial_100.net %t3models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T3_Partial_150.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Partial_150.net %t3models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T3_Partial_200.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Partial_200.net %t3models%

echo T = 4
echo 5
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T4_Partial_05.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Partial_05.net %t4models%
echo 10
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T4_Partial_10.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Partial_10.net %t4models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T4_Partial_20.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Partial_20.net %t4models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T4_Partial_100.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Partial_100.net %t4models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T4_Partial_150.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Partial_150.net %t4models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_simulations_T4_Partial_200.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Partial_200.net %t4models%





echo Filled In Policies
echo T = 3
echo 5
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T3_Filled_05.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Filled_05.net %t3models%
echo 10
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T3_Filled_10.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Filled_10.net %t3models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T3_Filled_20.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Filled_20.net %t3models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T3_Filled_100.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Filled_100.net %t3models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T3_Filled_150.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Filled_150.net %t3models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T3_Filled_200.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID3_Filled_200.net %t3models%

echo T = 4
echo 5
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T4_Filled_05.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Filled_05.net %t4models%
echo 10
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T4_Filled_10.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Filled_10.net %t4models%
echo 20
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T4_Filled_20.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Filled_20.net %t4models%
echo 100
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T4_Filled_100.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Filled_100.net %t4models%
echo 150
timeout /t 5
start cmd /C ..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T4_Filled_150.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Filled_150.net %t4models%
echo 200
timeout /t 5
..\HuginApiAddonsTesters.exe simulator %csvDir%I-DID_Simulations_T4_Filled_200.csv [s,u,oi,oj,ai,aj] %numSims% I-DID ..\nets\TIDID4_Filled_200.net %t4models%

